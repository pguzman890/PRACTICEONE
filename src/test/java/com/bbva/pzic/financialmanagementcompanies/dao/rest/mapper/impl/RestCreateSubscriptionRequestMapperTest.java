package com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.impl;

import com.bbva.pzic.financialmanagementcompanies.EntityStubs;
import com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests.ModelSubscriptionRequest;
import com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests.ModelSubscriptionResponse;
import com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.IRestCreateSubscriptionRequestMapper;
import com.bbva.pzic.financialmanagementcompanies.dao.rest.mock.stubs.ResponseFinancialManagementCompaniesMock;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.SubscriptionRequest;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

/**
 * Created on 03/08/2018.
 *
 * @author Entelgy
 */
public class RestCreateSubscriptionRequestMapperTest {

    private IRestCreateSubscriptionRequestMapper mapper;

    @Before
    public void setUp() {
        mapper = new RestCreateSubscriptionRequestMapper();
    }

    @Test
    public void mapInFullTest() throws IOException {
        SubscriptionRequest input = EntityStubs.getInstance().getSubscriptionRequest();

        ModelSubscriptionRequest result = mapper.mapIn(input);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getBusiness().getId());
        Assert.assertNotNull(result.getBusiness().getBusinessDocuments().get(0).getBusinessDocumentType().getId());
        Assert.assertNotNull(result.getBusiness().getBusinessDocuments().get(0).getDocumentNumber());
        Assert.assertNotNull(result.getBusiness().getBusinessManagement().getManagementType().getId());
        Assert.assertNotNull(result.getBusiness().getLegalName());
        Assert.assertNotNull(result.getLimitAmount().getAmount());
        Assert.assertNotNull(result.getLimitAmount().getCurrency());
        Assert.assertNotNull(result.getRelatedContracts().get(0).getContract().getId());
        Assert.assertNotNull(result.getRelatedContracts().get(0).getProduct().getId());
        Assert.assertNotNull(result.getRelatedContracts().get(0).getProduct().getProductType().getId());
        Assert.assertNotNull(result.getRelatedContracts().get(0).getRelationType().getId());
        Assert.assertNotNull(result.getProduct().getId());
        Assert.assertNotNull(result.getJoint().getId());
        Assert.assertNotNull(result.getBusinessManagers().get(0).getFirstName());
        Assert.assertNotNull(result.getBusinessManagers().get(0).getMiddleName());
        Assert.assertNotNull(result.getBusinessManagers().get(0).getLastName());
        Assert.assertNotNull(result.getBusinessManagers().get(0).getSecondLastName());
        Assert.assertNotNull(result.getBusinessManagers().get(0).getIdentityDocuments().get(0).getDocumentType().getId());
        Assert.assertNotNull(result.getBusinessManagers().get(0).getIdentityDocuments().get(0).getDocumentNumber());
        Assert.assertNotNull(result.getBusinessManagers().get(0).getContactDetails().get(0).getContactType());
        Assert.assertNotNull(result.getBusinessManagers().get(0).getContactDetails().get(0).getContact());
        Assert.assertNotNull(result.getBusinessManagers().get(0).getRoles().get(0).getId());
        Assert.assertNotNull(result.getUnitManagement());
        Assert.assertNotNull(result.getBranch().getId());

        Assert.assertEquals(input.getBusiness().getId(), result.getBusiness().getId());
        Assert.assertEquals(input.getBusiness().getBusinessDocuments().get(0).getBusinessDocumentType().getId(), result.getBusiness().getBusinessDocuments().get(0).getBusinessDocumentType().getId());
        Assert.assertEquals(input.getBusiness().getBusinessDocuments().get(0).getDocumentNumber(), result.getBusiness().getBusinessDocuments().get(0).getDocumentNumber());
        Assert.assertEquals(input.getBusiness().getBusinessManagement().getManagementType().getId(), result.getBusiness().getBusinessManagement().getManagementType().getId());
        Assert.assertEquals(input.getBusiness().getLegalName(), result.getBusiness().getLegalName());
        Assert.assertEquals(input.getLimitAmount().getAmount(), result.getLimitAmount().getAmount());
        Assert.assertEquals(input.getLimitAmount().getCurrency(), result.getLimitAmount().getCurrency());
        Assert.assertEquals(input.getRelatedContracts().get(0).getContract().getId(), result.getRelatedContracts().get(0).getContract().getId());
        Assert.assertEquals(input.getRelatedContracts().get(0).getProduct().getId(), result.getRelatedContracts().get(0).getProduct().getId());
        Assert.assertEquals(input.getRelatedContracts().get(0).getProduct().getProductType().getId(), result.getRelatedContracts().get(0).getProduct().getProductType().getId());
        Assert.assertEquals(input.getRelatedContracts().get(0).getRelationType().getId(), result.getRelatedContracts().get(0).getRelationType().getId());
        Assert.assertEquals(input.getProduct().getId(), result.getProduct().getId());
        Assert.assertEquals(input.getJoint().getId(), result.getJoint().getId());
        Assert.assertEquals(input.getBusinessManagers().get(0).getFirstName(), result.getBusinessManagers().get(0).getFirstName());
        Assert.assertEquals(input.getBusinessManagers().get(0).getMiddleName(), result.getBusinessManagers().get(0).getMiddleName());
        Assert.assertEquals(input.getBusinessManagers().get(0).getLastName(), result.getBusinessManagers().get(0).getLastName());
        Assert.assertEquals(input.getBusinessManagers().get(0).getSecondLastName(), result.getBusinessManagers().get(0).getSecondLastName());
        Assert.assertEquals(input.getBusinessManagers().get(0).getIdentityDocuments().get(0).getDocumentType().getId(), result.getBusinessManagers().get(0).getIdentityDocuments().get(0).getDocumentType().getId());
        Assert.assertEquals(input.getBusinessManagers().get(0).getIdentityDocuments().get(0).getDocumentNumber(), result.getBusinessManagers().get(0).getIdentityDocuments().get(0).getDocumentNumber());
        Assert.assertEquals(input.getBusinessManagers().get(0).getContactDetails().get(0).getContactType(), result.getBusinessManagers().get(0).getContactDetails().get(0).getContactType());
        Assert.assertEquals(input.getBusinessManagers().get(0).getContactDetails().get(0).getContact(), result.getBusinessManagers().get(0).getContactDetails().get(0).getContact());
        Assert.assertEquals(input.getBusinessManagers().get(0).getRoles().get(0).getId(), result.getBusinessManagers().get(0).getRoles().get(0).getId());
        Assert.assertEquals(input.getUnitManagement(), result.getUnitManagement());
        Assert.assertEquals(input.getBranch().getId(), result.getBranch().getId());
    }

    @Test
    public void mapInFullWithoutIdAndLegalNameFromBusinessTest() throws IOException {
        SubscriptionRequest input = EntityStubs.getInstance()
                .getSubscriptionRequest();
        input.getBusiness().setId(null);
        input.getBusiness().setLegalName(null);

        ModelSubscriptionRequest result = mapper.mapIn(input);
        Assert.assertNotNull(result);
        Assert.assertNull(result.getBusiness().getId());
        Assert.assertNotNull(result.getBusiness().getBusinessDocuments().get(0)
                .getBusinessDocumentType().getId());
        Assert.assertNotNull(result.getBusiness().getBusinessDocuments().get(0)
                .getDocumentNumber());
        Assert.assertNull(result.getBusiness().getLegalName());
        Assert.assertNotNull(result.getLimitAmount().getAmount());
        Assert.assertNotNull(result.getLimitAmount().getCurrency());
        Assert.assertNotNull(result.getRelatedContracts().get(0).getContract()
                .getId());
        Assert.assertNotNull(result.getRelatedContracts().get(0).getProduct()
                .getId());
        Assert.assertNotNull(result.getRelatedContracts().get(0).getProduct()
                .getProductType().getId());
        Assert.assertNotNull(result.getRelatedContracts().get(0)
                .getRelationType().getId());
        Assert.assertNotNull(result.getProduct().getId());
        Assert.assertNotNull(result.getJoint().getId());
        Assert.assertNotNull(result.getBusinessManagers().get(0).getFirstName());
        Assert.assertNotNull(result.getBusinessManagers().get(0)
                .getMiddleName());
        Assert.assertNotNull(result.getBusinessManagers().get(0).getLastName());
        Assert.assertNotNull(result.getBusinessManagers().get(0)
                .getSecondLastName());
        Assert.assertNotNull(result.getBusinessManagers().get(0)
                .getIdentityDocuments().get(0).getDocumentType().getId());
        Assert.assertNotNull(result.getBusinessManagers().get(0)
                .getIdentityDocuments().get(0).getDocumentNumber());
        Assert.assertNotNull(result.getBusinessManagers().get(0)
                .getContactDetails().get(0).getContactType());
        Assert.assertNotNull(result.getBusinessManagers().get(0)
                .getContactDetails().get(0).getContact());
        Assert.assertNotNull(result.getBusinessManagers().get(0).getRoles()
                .get(0).getId());
        Assert.assertNotNull(result.getUnitManagement());
        Assert.assertNotNull(result.getBranch().getId());

        Assert.assertEquals(input.getBusiness().getId(), result.getBusiness()
                .getId());
        Assert.assertEquals(input.getBusiness()
                .getBusinessDocuments().get(0).getBusinessDocumentType()
                .getId(), result.getBusiness()
                .getBusinessDocuments().get(0).getBusinessDocumentType()
                .getId());
        Assert.assertEquals(input.getBusiness().getBusinessDocuments().get(0)
                .getDocumentNumber(), result.getBusiness()
                .getBusinessDocuments().get(0).getDocumentNumber());
        Assert.assertEquals(input.getBusiness().getLegalName(), result
                .getBusiness().getLegalName());
        Assert.assertEquals(input.getLimitAmount().getAmount(), result
                .getLimitAmount().getAmount());
        Assert.assertEquals(input.getLimitAmount().getCurrency(), result
                .getLimitAmount().getCurrency());
        Assert.assertEquals(input.getRelatedContracts().get(0).getContract()
                .getId(), result.getRelatedContracts().get(0).getContract()
                .getId());
        Assert.assertEquals(input.getRelatedContracts().get(0).getProduct()
                .getId(), result.getRelatedContracts().get(0).getProduct()
                .getId());
        Assert.assertEquals(input.getRelatedContracts().get(0)
                .getProduct().getProductType().getId(), result.getRelatedContracts().get(0)
                .getProduct().getProductType().getId());
        Assert.assertEquals(input.getRelatedContracts().get(0)
                .getRelationType().getId(), result.getRelatedContracts().get(0)
                .getRelationType().getId());
        Assert.assertEquals(input.getProduct()
                .getId(), result.getProduct()
                .getId());
        Assert.assertEquals(input.getJoint().getId(), result.getJoint().getId());
        Assert.assertEquals(input.getBusinessManagers().get(0).getFirstName(),
                result.getBusinessManagers().get(0).getFirstName());
        Assert.assertEquals(input.getBusinessManagers().get(0).getMiddleName(),
                result.getBusinessManagers().get(0).getMiddleName());
        Assert.assertEquals(input.getBusinessManagers().get(0).getLastName(),
                result.getBusinessManagers().get(0).getLastName());
        Assert.assertEquals(input.getBusinessManagers().get(0)
                .getSecondLastName(), result.getBusinessManagers().get(0)
                .getSecondLastName());
        Assert.assertEquals(input.getBusinessManagers().get(0).getIdentityDocuments()
                        .get(0).getDocumentType().getId(),
                result.getBusinessManagers().get(0).getIdentityDocuments()
                        .get(0).getDocumentType().getId());
        Assert.assertEquals(input.getBusinessManagers().get(0)
                .getIdentityDocuments().get(0).getDocumentNumber(), result
                .getBusinessManagers().get(0).getIdentityDocuments().get(0)
                .getDocumentNumber());
        Assert.assertEquals(input
                .getBusinessManagers().get(0).getContactDetails().get(0)
                .getContactType(), result
                .getBusinessManagers().get(0).getContactDetails().get(0)
                .getContactType());
        Assert.assertEquals(input.getBusinessManagers().get(0)
                .getContactDetails().get(0).getContact(), result
                .getBusinessManagers().get(0).getContactDetails().get(0)
                .getContact());
        Assert.assertEquals(input.getBusinessManagers().get(0).getRoles().get(0)
                .getId(), result.getBusinessManagers().get(0).getRoles().get(0)
                .getId());
        Assert.assertEquals(input.getUnitManagement(),
                result.getUnitManagement());
        Assert.assertEquals(input.getBranch().getId(), result.getBranch()
                .getId());
    }

    @Test
    public void mapInWithoutBusinessDocumentsTest() throws IOException {
        SubscriptionRequest input = EntityStubs.getInstance()
                .getSubscriptionRequest();
        input.getBusiness().setBusinessDocuments(null);
        ModelSubscriptionRequest result = mapper.mapIn(input);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getBusiness().getId());
        Assert.assertNull(result.getBusiness().getBusinessDocuments());
        Assert.assertNotNull(result.getBusiness().getLegalName());
        Assert.assertNotNull(result.getLimitAmount().getAmount());
        Assert.assertNotNull(result.getLimitAmount().getCurrency());
        Assert.assertNotNull(result.getRelatedContracts().get(0).getContract()
                .getId());
        Assert.assertNotNull(result.getRelatedContracts().get(0).getProduct()
                .getId());
        Assert.assertNotNull(result.getRelatedContracts().get(0).getProduct()
                .getProductType().getId());
        Assert.assertNotNull(result.getRelatedContracts().get(0)
                .getRelationType().getId());
        Assert.assertNotNull(result.getProduct().getId());
        Assert.assertNotNull(result.getJoint().getId());
        Assert.assertNotNull(result.getBusinessManagers().get(0).getFirstName());
        Assert.assertNotNull(result.getBusinessManagers().get(0)
                .getMiddleName());
        Assert.assertNotNull(result.getBusinessManagers().get(0).getLastName());
        Assert.assertNotNull(result.getBusinessManagers().get(0)
                .getSecondLastName());
        Assert.assertNotNull(result.getBusinessManagers().get(0)
                .getIdentityDocuments().get(0).getDocumentType().getId());
        Assert.assertNotNull(result.getBusinessManagers().get(0)
                .getIdentityDocuments().get(0).getDocumentNumber());
        Assert.assertNotNull(result.getBusinessManagers().get(0)
                .getContactDetails().get(0).getContactType());
        Assert.assertNotNull(result.getBusinessManagers().get(0)
                .getContactDetails().get(0).getContact());
        Assert.assertNotNull(result.getBusinessManagers().get(0).getRoles()
                .get(0).getId());
        Assert.assertNotNull(result.getUnitManagement());
        Assert.assertNotNull(result.getBranch().getId());

        Assert.assertEquals(input.getBusiness().getId(), result.getBusiness()
                .getId());

        Assert.assertEquals(input.getBusiness().getLegalName(), result
                .getBusiness().getLegalName());
        Assert.assertEquals(input.getLimitAmount().getAmount(), result
                .getLimitAmount().getAmount());
        Assert.assertEquals(input.getLimitAmount().getCurrency(), result
                .getLimitAmount().getCurrency());
        Assert.assertEquals(input.getRelatedContracts().get(0).getContract()
                .getId(), result.getRelatedContracts().get(0).getContract()
                .getId());
        Assert.assertEquals(input.getRelatedContracts().get(0).getProduct()
                .getId(), result.getRelatedContracts().get(0).getProduct()
                .getId());
        Assert.assertEquals(input.getRelatedContracts().get(0)
                .getProduct().getProductType().getId(), result.getRelatedContracts().get(0)
                .getProduct().getProductType().getId());
        Assert.assertEquals(input.getRelatedContracts().get(0)
                .getRelationType().getId(), result.getRelatedContracts().get(0)
                .getRelationType().getId());
        Assert.assertEquals(input.getProduct()
                .getId(), result.getProduct()
                .getId());
        Assert.assertEquals(input.getJoint().getId(), result.getJoint().getId());
        Assert.assertEquals(input.getBusinessManagers().get(0).getFirstName(),
                result.getBusinessManagers().get(0).getFirstName());
        Assert.assertEquals(input.getBusinessManagers().get(0).getMiddleName(),
                result.getBusinessManagers().get(0).getMiddleName());
        Assert.assertEquals(input.getBusinessManagers().get(0).getLastName(),
                result.getBusinessManagers().get(0).getLastName());
        Assert.assertEquals(input.getBusinessManagers().get(0)
                .getSecondLastName(), result.getBusinessManagers().get(0)
                .getSecondLastName());
        Assert.assertEquals(input.getBusinessManagers().get(0).getIdentityDocuments()
                        .get(0).getDocumentType().getId(),
                result.getBusinessManagers().get(0).getIdentityDocuments()
                        .get(0).getDocumentType().getId());
        Assert.assertEquals(input.getBusinessManagers().get(0)
                .getIdentityDocuments().get(0).getDocumentNumber(), result
                .getBusinessManagers().get(0).getIdentityDocuments().get(0)
                .getDocumentNumber());
        Assert.assertEquals(input
                .getBusinessManagers().get(0).getContactDetails().get(0)
                .getContactType(), result
                .getBusinessManagers().get(0).getContactDetails().get(0)
                .getContactType());
        Assert.assertEquals(input.getBusinessManagers().get(0)
                .getContactDetails().get(0).getContact(), result
                .getBusinessManagers().get(0).getContactDetails().get(0)
                .getContact());
        Assert.assertEquals(input.getBusinessManagers().get(0).getRoles().get(0)
                .getId(), result.getBusinessManagers().get(0).getRoles().get(0)
                .getId());
        Assert.assertEquals(input.getUnitManagement(),
                result.getUnitManagement());
        Assert.assertEquals(input.getBranch().getId(), result.getBranch()
                .getId());
    }

    @Test
    public void mapInWithoutBusinessTest() throws IOException {
        SubscriptionRequest input = EntityStubs.getInstance()
                .getSubscriptionRequest();
        input.setBusiness(null);
        ModelSubscriptionRequest result = mapper.mapIn(input);
        Assert.assertNotNull(result);
        Assert.assertNull(result.getBusiness());
        Assert.assertNotNull(result.getLimitAmount().getAmount());
        Assert.assertNotNull(result.getLimitAmount().getCurrency());
        Assert.assertNotNull(result.getRelatedContracts().get(0).getContract()
                .getId());
        Assert.assertNotNull(result.getRelatedContracts().get(0).getProduct()
                .getId());
        Assert.assertNotNull(result.getRelatedContracts().get(0).getProduct()
                .getProductType().getId());
        Assert.assertNotNull(result.getRelatedContracts().get(0)
                .getRelationType().getId());
        Assert.assertNotNull(result.getProduct().getId());
        Assert.assertNotNull(result.getJoint().getId());
        Assert.assertNotNull(result.getBusinessManagers().get(0).getFirstName());
        Assert.assertNotNull(result.getBusinessManagers().get(0)
                .getMiddleName());
        Assert.assertNotNull(result.getBusinessManagers().get(0).getLastName());
        Assert.assertNotNull(result.getBusinessManagers().get(0)
                .getSecondLastName());
        Assert.assertNotNull(result.getBusinessManagers().get(0)
                .getIdentityDocuments().get(0).getDocumentType().getId());
        Assert.assertNotNull(result.getBusinessManagers().get(0)
                .getIdentityDocuments().get(0).getDocumentNumber());
        Assert.assertNotNull(result.getBusinessManagers().get(0)
                .getContactDetails().get(0).getContactType());
        Assert.assertNotNull(result.getBusinessManagers().get(0)
                .getContactDetails().get(0).getContact());
        Assert.assertNotNull(result.getBusinessManagers().get(0).getRoles()
                .get(0).getId());
        Assert.assertNotNull(result.getUnitManagement());
        Assert.assertNotNull(result.getBranch().getId());

        Assert.assertEquals(input.getLimitAmount().getAmount(), result
                .getLimitAmount().getAmount());
        Assert.assertEquals(input.getLimitAmount().getCurrency(), result
                .getLimitAmount().getCurrency());
        Assert.assertEquals(input.getRelatedContracts().get(0).getContract()
                .getId(), result.getRelatedContracts().get(0).getContract()
                .getId());
        Assert.assertEquals(input.getRelatedContracts().get(0).getProduct()
                .getId(), result.getRelatedContracts().get(0).getProduct()
                .getId());
        Assert.assertEquals(input.getRelatedContracts().get(0)
                .getProduct().getProductType().getId(), result.getRelatedContracts().get(0)
                .getProduct().getProductType().getId());
        Assert.assertEquals(input.getRelatedContracts().get(0)
                .getRelationType().getId(), result.getRelatedContracts().get(0)
                .getRelationType().getId());
        Assert.assertEquals(input.getProduct()
                .getId(), result.getProduct()
                .getId());
        Assert.assertEquals(input.getJoint().getId(), result.getJoint().getId());
        Assert.assertEquals(input.getBusinessManagers().get(0).getFirstName(),
                result.getBusinessManagers().get(0).getFirstName());
        Assert.assertEquals(input.getBusinessManagers().get(0).getMiddleName(),
                result.getBusinessManagers().get(0).getMiddleName());
        Assert.assertEquals(input.getBusinessManagers().get(0).getLastName(),
                result.getBusinessManagers().get(0).getLastName());
        Assert.assertEquals(input.getBusinessManagers().get(0)
                .getSecondLastName(), result.getBusinessManagers().get(0)
                .getSecondLastName());
        Assert.assertEquals(input.getBusinessManagers().get(0).getIdentityDocuments()
                        .get(0).getDocumentType().getId(),
                result.getBusinessManagers().get(0).getIdentityDocuments()
                        .get(0).getDocumentType().getId());
        Assert.assertEquals(input.getBusinessManagers().get(0)
                .getIdentityDocuments().get(0).getDocumentNumber(), result
                .getBusinessManagers().get(0).getIdentityDocuments().get(0)
                .getDocumentNumber());
        Assert.assertEquals(input
                .getBusinessManagers().get(0).getContactDetails().get(0)
                .getContactType(), result
                .getBusinessManagers().get(0).getContactDetails().get(0)
                .getContactType());
        Assert.assertEquals(input.getBusinessManagers().get(0)
                .getContactDetails().get(0).getContact(), result
                .getBusinessManagers().get(0).getContactDetails().get(0)
                .getContact());
        Assert.assertEquals(input.getBusinessManagers().get(0).getRoles().get(0)
                .getId(), result.getBusinessManagers().get(0).getRoles().get(0)
                .getId());
        Assert.assertEquals(input.getUnitManagement(),
                result.getUnitManagement());
        Assert.assertEquals(input.getBranch().getId(), result.getBranch()
                .getId());
    }

    @Test
    public void mapInWithoutRelatedContractsTest() throws IOException {
        SubscriptionRequest input = EntityStubs.getInstance()
                .getSubscriptionRequest();
        input.setRelatedContracts(null);

        ModelSubscriptionRequest result = mapper.mapIn(input);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getBusiness().getId());
        Assert.assertNotNull(result.getBusiness().getBusinessDocuments().get(0)
                .getBusinessDocumentType().getId());
        Assert.assertNotNull(result.getBusiness().getBusinessDocuments().get(0)
                .getDocumentNumber());
        Assert.assertNotNull(result.getBusiness().getLegalName());
        Assert.assertNotNull(result.getLimitAmount().getAmount());
        Assert.assertNotNull(result.getLimitAmount().getCurrency());
        Assert.assertNull(result.getRelatedContracts());
        Assert.assertNotNull(result.getProduct().getId());
        Assert.assertNotNull(result.getJoint().getId());
        Assert.assertNotNull(result.getBusinessManagers().get(0).getFirstName());
        Assert.assertNotNull(result.getBusinessManagers().get(0)
                .getMiddleName());
        Assert.assertNotNull(result.getBusinessManagers().get(0).getLastName());
        Assert.assertNotNull(result.getBusinessManagers().get(0)
                .getSecondLastName());
        Assert.assertNotNull(result.getBusinessManagers().get(0)
                .getIdentityDocuments().get(0).getDocumentType().getId());
        Assert.assertNotNull(result.getBusinessManagers().get(0)
                .getIdentityDocuments().get(0).getDocumentNumber());
        Assert.assertNotNull(result.getBusinessManagers().get(0)
                .getContactDetails().get(0).getContactType());
        Assert.assertNotNull(result.getBusinessManagers().get(0)
                .getContactDetails().get(0).getContact());
        Assert.assertNotNull(result.getBusinessManagers().get(0).getRoles()
                .get(0).getId());
        Assert.assertNotNull(result.getUnitManagement());
        Assert.assertNotNull(result.getBranch().getId());

        Assert.assertEquals(input.getBusiness().getId(), result.getBusiness()
                .getId());
        Assert.assertEquals(input.getBusiness()
                .getBusinessDocuments().get(0).getBusinessDocumentType()
                .getId(), result.getBusiness()
                .getBusinessDocuments().get(0).getBusinessDocumentType()
                .getId());
        Assert.assertEquals(input.getBusiness().getBusinessDocuments().get(0)
                .getDocumentNumber(), result.getBusiness()
                .getBusinessDocuments().get(0).getDocumentNumber());
        Assert.assertEquals(input.getBusiness().getLegalName(), result
                .getBusiness().getLegalName());
        Assert.assertEquals(input.getLimitAmount().getAmount(), result
                .getLimitAmount().getAmount());
        Assert.assertEquals(input.getLimitAmount().getCurrency(), result
                .getLimitAmount().getCurrency());
        Assert.assertEquals(input.getProduct()
                .getId(), result.getProduct()
                .getId());
        Assert.assertEquals(input.getJoint().getId(), result.getJoint().getId());
        Assert.assertEquals(input.getBusinessManagers().get(0).getFirstName(),
                result.getBusinessManagers().get(0).getFirstName());
        Assert.assertEquals(input.getBusinessManagers().get(0).getMiddleName(),
                result.getBusinessManagers().get(0).getMiddleName());
        Assert.assertEquals(input.getBusinessManagers().get(0).getLastName(),
                result.getBusinessManagers().get(0).getLastName());
        Assert.assertEquals(input.getBusinessManagers().get(0)
                .getSecondLastName(), result.getBusinessManagers().get(0)
                .getSecondLastName());
        Assert.assertEquals(input.getBusinessManagers().get(0).getIdentityDocuments()
                        .get(0).getDocumentType().getId(),
                result.getBusinessManagers().get(0).getIdentityDocuments()
                        .get(0).getDocumentType().getId());
        Assert.assertEquals(input.getBusinessManagers().get(0)
                .getIdentityDocuments().get(0).getDocumentNumber(), result
                .getBusinessManagers().get(0).getIdentityDocuments().get(0)
                .getDocumentNumber());
        Assert.assertEquals(input
                .getBusinessManagers().get(0).getContactDetails().get(0)
                .getContactType(), result
                .getBusinessManagers().get(0).getContactDetails().get(0)
                .getContactType());
        Assert.assertEquals(input.getBusinessManagers().get(0)
                .getContactDetails().get(0).getContact(), result
                .getBusinessManagers().get(0).getContactDetails().get(0)
                .getContact());
        Assert.assertEquals(input.getBusinessManagers().get(0).getRoles().get(0)
                .getId(), result.getBusinessManagers().get(0).getRoles().get(0)
                .getId());
        Assert.assertEquals(input.getUnitManagement(),
                result.getUnitManagement());
        Assert.assertEquals(input.getBranch().getId(), result.getBranch()
                .getId());
    }

    @Test
    public void mapInWithoutBusinessManagersTest() throws IOException {
        SubscriptionRequest input = EntityStubs.getInstance()
                .getSubscriptionRequest();
        input.setBusinessManagers(null);

        ModelSubscriptionRequest result = mapper.mapIn(input);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getBusiness().getId());
        Assert.assertNotNull(result.getBusiness().getBusinessDocuments().get(0)
                .getBusinessDocumentType().getId());
        Assert.assertNotNull(result.getBusiness().getBusinessDocuments().get(0)
                .getDocumentNumber());
        Assert.assertNotNull(result.getBusiness().getLegalName());
        Assert.assertNotNull(result.getLimitAmount().getAmount());
        Assert.assertNotNull(result.getLimitAmount().getCurrency());
        Assert.assertNotNull(result.getRelatedContracts().get(0).getContract()
                .getId());
        Assert.assertNotNull(result.getRelatedContracts().get(0).getProduct()
                .getId());
        Assert.assertNotNull(result.getRelatedContracts().get(0).getProduct()
                .getProductType().getId());
        Assert.assertNotNull(result.getRelatedContracts().get(0)
                .getRelationType().getId());
        Assert.assertNotNull(result.getProduct().getId());
        Assert.assertNotNull(result.getJoint().getId());
        Assert.assertNull(result.getBusinessManagers());
        Assert.assertNotNull(result.getUnitManagement());
        Assert.assertNotNull(result.getBranch().getId());

        Assert.assertEquals(input.getBusiness().getId(), result.getBusiness()
                .getId());
        Assert.assertEquals(input.getBusiness()
                .getBusinessDocuments().get(0).getBusinessDocumentType()
                .getId(), result.getBusiness()
                .getBusinessDocuments().get(0).getBusinessDocumentType()
                .getId());
        Assert.assertEquals(input.getBusiness().getBusinessDocuments().get(0)
                .getDocumentNumber(), result.getBusiness()
                .getBusinessDocuments().get(0).getDocumentNumber());
        Assert.assertEquals(input.getBusiness().getLegalName(), result
                .getBusiness().getLegalName());
        Assert.assertEquals(input.getLimitAmount().getAmount(), result
                .getLimitAmount().getAmount());
        Assert.assertEquals(input.getLimitAmount().getCurrency(), result
                .getLimitAmount().getCurrency());
        Assert.assertEquals(input.getRelatedContracts().get(0).getContract()
                .getId(), result.getRelatedContracts().get(0).getContract()
                .getId());
        Assert.assertEquals(input.getRelatedContracts().get(0).getProduct()
                .getId(), result.getRelatedContracts().get(0).getProduct()
                .getId());
        Assert.assertEquals(input.getRelatedContracts().get(0)
                .getProduct().getProductType().getId(), result.getRelatedContracts().get(0)
                .getProduct().getProductType().getId());
        Assert.assertEquals(input.getRelatedContracts().get(0)
                .getRelationType().getId(), result.getRelatedContracts().get(0)
                .getRelationType().getId());
        Assert.assertEquals(input.getProduct()
                .getId(), result.getProduct()
                .getId());
        Assert.assertEquals(input.getJoint().getId(), result.getJoint().getId());

        Assert.assertEquals(input.getUnitManagement(),
                result.getUnitManagement());
        Assert.assertEquals(input.getBranch().getId(), result.getBranch()
                .getId());
    }

    @Test
    public void mapInWithoutFirstIdentityDocumentsFromBusinessManagersTest() throws IOException {
        SubscriptionRequest input = EntityStubs.getInstance()
                .getSubscriptionRequest();
        input.getBusinessManagers().get(0).setIdentityDocuments(null);

        ModelSubscriptionRequest result = mapper.mapIn(input);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getBusiness().getId());
        Assert.assertNotNull(result.getBusiness().getBusinessDocuments().get(0)
                .getBusinessDocumentType().getId());
        Assert.assertNotNull(result.getBusiness().getBusinessDocuments().get(0)
                .getDocumentNumber());
        Assert.assertNotNull(result.getBusiness().getLegalName());
        Assert.assertNotNull(result.getLimitAmount().getAmount());
        Assert.assertNotNull(result.getLimitAmount().getCurrency());
        Assert.assertNotNull(result.getRelatedContracts().get(0).getContract()
                .getId());
        Assert.assertNotNull(result.getRelatedContracts().get(0).getProduct()
                .getId());
        Assert.assertNotNull(result.getRelatedContracts().get(0).getProduct()
                .getProductType().getId());
        Assert.assertNotNull(result.getRelatedContracts().get(0)
                .getRelationType().getId());
        Assert.assertNotNull(result.getProduct().getId());
        Assert.assertNotNull(result.getJoint().getId());
        Assert.assertNotNull(result.getBusinessManagers().get(0).getFirstName());
        Assert.assertNotNull(result.getBusinessManagers().get(0)
                .getMiddleName());
        Assert.assertNotNull(result.getBusinessManagers().get(0).getLastName());
        Assert.assertNotNull(result.getBusinessManagers().get(0)
                .getSecondLastName());
        Assert.assertNull(result.getBusinessManagers().get(0)
                .getIdentityDocuments());
        Assert.assertNotNull(result.getBusinessManagers().get(0)
                .getContactDetails().get(0).getContactType());
        Assert.assertNotNull(result.getBusinessManagers().get(0)
                .getContactDetails().get(0).getContact());
        Assert.assertNotNull(result.getBusinessManagers().get(0).getRoles()
                .get(0).getId());
        Assert.assertNotNull(result.getUnitManagement());
        Assert.assertNotNull(result.getBranch().getId());

        Assert.assertEquals(input.getBusiness().getId(), result.getBusiness()
                .getId());
        Assert.assertEquals(input.getBusiness()
                .getBusinessDocuments().get(0).getBusinessDocumentType()
                .getId(), result.getBusiness()
                .getBusinessDocuments().get(0).getBusinessDocumentType()
                .getId());
        Assert.assertEquals(input.getBusiness().getBusinessDocuments().get(0)
                .getDocumentNumber(), result.getBusiness()
                .getBusinessDocuments().get(0).getDocumentNumber());
        Assert.assertEquals(input.getBusiness().getLegalName(), result
                .getBusiness().getLegalName());
        Assert.assertEquals(input.getLimitAmount().getAmount(), result
                .getLimitAmount().getAmount());
        Assert.assertEquals(input.getLimitAmount().getCurrency(), result
                .getLimitAmount().getCurrency());
        Assert.assertEquals(input.getRelatedContracts().get(0).getContract()
                .getId(), result.getRelatedContracts().get(0).getContract()
                .getId());
        Assert.assertEquals(input.getRelatedContracts().get(0).getProduct()
                .getId(), result.getRelatedContracts().get(0).getProduct()
                .getId());
        Assert.assertEquals(input.getRelatedContracts().get(0)
                .getProduct().getProductType().getId(), result.getRelatedContracts().get(0)
                .getProduct().getProductType().getId());
        Assert.assertEquals(input.getRelatedContracts().get(0)
                .getRelationType().getId(), result.getRelatedContracts().get(0)
                .getRelationType().getId());
        Assert.assertEquals(input.getProduct()
                .getId(), result.getProduct()
                .getId());
        Assert.assertEquals(input.getJoint().getId(), result.getJoint().getId());
        Assert.assertEquals(input.getBusinessManagers().get(0).getFirstName(),
                result.getBusinessManagers().get(0).getFirstName());
        Assert.assertEquals(input.getBusinessManagers().get(0).getMiddleName(),
                result.getBusinessManagers().get(0).getMiddleName());
        Assert.assertEquals(input.getBusinessManagers().get(0).getLastName(),
                result.getBusinessManagers().get(0).getLastName());
        Assert.assertEquals(input.getBusinessManagers().get(0)
                .getSecondLastName(), result.getBusinessManagers().get(0)
                .getSecondLastName());

        Assert.assertEquals(input
                .getBusinessManagers().get(0).getContactDetails().get(0)
                .getContactType(), result
                .getBusinessManagers().get(0).getContactDetails().get(0)
                .getContactType());
        Assert.assertEquals(input.getBusinessManagers().get(0)
                .getContactDetails().get(0).getContact(), result
                .getBusinessManagers().get(0).getContactDetails().get(0)
                .getContact());
        Assert.assertEquals(input.getBusinessManagers().get(0).getRoles().get(0)
                .getId(), result.getBusinessManagers().get(0).getRoles().get(0)
                .getId());
        Assert.assertEquals(input.getUnitManagement(),
                result.getUnitManagement());
        Assert.assertEquals(input.getBranch().getId(), result.getBranch()
                .getId());
    }

    @Test
    public void mapInWithoutFirstContactDetailsFromBusinessManagersTest() throws IOException {
        SubscriptionRequest input = EntityStubs.getInstance()
                .getSubscriptionRequest();
        input.getBusinessManagers().get(0).setContactDetails(null);

        ModelSubscriptionRequest result = mapper.mapIn(input);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getBusiness().getId());
        Assert.assertNotNull(result.getBusiness().getBusinessDocuments().get(0)
                .getBusinessDocumentType().getId());
        Assert.assertNotNull(result.getBusiness().getBusinessDocuments().get(0)
                .getDocumentNumber());
        Assert.assertNotNull(result.getBusiness().getLegalName());
        Assert.assertNotNull(result.getLimitAmount().getAmount());
        Assert.assertNotNull(result.getLimitAmount().getCurrency());
        Assert.assertNotNull(result.getRelatedContracts().get(0).getContract()
                .getId());
        Assert.assertNotNull(result.getRelatedContracts().get(0).getProduct()
                .getId());
        Assert.assertNotNull(result.getRelatedContracts().get(0).getProduct()
                .getProductType().getId());
        Assert.assertNotNull(result.getRelatedContracts().get(0)
                .getRelationType().getId());
        Assert.assertNotNull(result.getProduct().getId());
        Assert.assertNotNull(result.getJoint().getId());
        Assert.assertNotNull(result.getBusinessManagers().get(0).getFirstName());
        Assert.assertNotNull(result.getBusinessManagers().get(0)
                .getMiddleName());
        Assert.assertNotNull(result.getBusinessManagers().get(0).getLastName());
        Assert.assertNotNull(result.getBusinessManagers().get(0)
                .getSecondLastName());
        Assert.assertNotNull(result.getBusinessManagers().get(0)
                .getIdentityDocuments().get(0).getDocumentType().getId());
        Assert.assertNotNull(result.getBusinessManagers().get(0)
                .getIdentityDocuments().get(0).getDocumentNumber());
        Assert.assertNull(result.getBusinessManagers().get(0)
                .getContactDetails());
        Assert.assertNotNull(result.getBusinessManagers().get(0).getRoles()
                .get(0).getId());
        Assert.assertNotNull(result.getUnitManagement());
        Assert.assertNotNull(result.getBranch().getId());

        Assert.assertEquals(input.getBusiness().getId(), result.getBusiness()
                .getId());
        Assert.assertEquals(input.getBusiness()
                .getBusinessDocuments().get(0).getBusinessDocumentType()
                .getId(), result.getBusiness()
                .getBusinessDocuments().get(0).getBusinessDocumentType()
                .getId());
        Assert.assertEquals(input.getBusiness().getBusinessDocuments().get(0)
                .getDocumentNumber(), result.getBusiness()
                .getBusinessDocuments().get(0).getDocumentNumber());
        Assert.assertEquals(input.getBusiness().getLegalName(), result
                .getBusiness().getLegalName());
        Assert.assertEquals(input.getLimitAmount().getAmount(), result
                .getLimitAmount().getAmount());
        Assert.assertEquals(input.getLimitAmount().getCurrency(), result
                .getLimitAmount().getCurrency());
        Assert.assertEquals(input.getRelatedContracts().get(0).getContract()
                .getId(), result.getRelatedContracts().get(0).getContract()
                .getId());
        Assert.assertEquals(input.getRelatedContracts().get(0).getProduct()
                .getId(), result.getRelatedContracts().get(0).getProduct()
                .getId());
        Assert.assertEquals(input.getRelatedContracts().get(0)
                .getProduct().getProductType().getId(), result.getRelatedContracts().get(0)
                .getProduct().getProductType().getId());
        Assert.assertEquals(input.getRelatedContracts().get(0)
                .getRelationType().getId(), result.getRelatedContracts().get(0)
                .getRelationType().getId());
        Assert.assertEquals(input.getProduct()
                .getId(), result.getProduct()
                .getId());
        Assert.assertEquals(input.getJoint().getId(), result.getJoint().getId());
        Assert.assertEquals(input.getBusinessManagers().get(0).getFirstName(),
                result.getBusinessManagers().get(0).getFirstName());
        Assert.assertEquals(input.getBusinessManagers().get(0).getMiddleName(),
                result.getBusinessManagers().get(0).getMiddleName());
        Assert.assertEquals(input.getBusinessManagers().get(0).getLastName(),
                result.getBusinessManagers().get(0).getLastName());
        Assert.assertEquals(input.getBusinessManagers().get(0)
                .getSecondLastName(), result.getBusinessManagers().get(0)
                .getSecondLastName());
        Assert.assertEquals(input.getBusinessManagers().get(0).getIdentityDocuments()
                        .get(0).getDocumentType().getId(),
                result.getBusinessManagers().get(0).getIdentityDocuments()
                        .get(0).getDocumentType().getId());
        Assert.assertEquals(input.getBusinessManagers().get(0)
                .getIdentityDocuments().get(0).getDocumentNumber(), result
                .getBusinessManagers().get(0).getIdentityDocuments().get(0)
                .getDocumentNumber());

        Assert.assertEquals(input.getBusinessManagers().get(0).getRoles().get(0)
                .getId(), result.getBusinessManagers().get(0).getRoles().get(0)
                .getId());
        Assert.assertEquals(input.getUnitManagement(),
                result.getUnitManagement());
        Assert.assertEquals(input.getBranch().getId(), result.getBranch()
                .getId());
    }

    @Test
    public void mapInWithoutFirstRoleFromBusinessManagerTest() throws IOException {
        SubscriptionRequest input = EntityStubs.getInstance()
                .getSubscriptionRequest();
        input.getBusinessManagers().get(0).setRoles(null);

        ModelSubscriptionRequest result = mapper.mapIn(input);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getBusiness().getId());
        Assert.assertNotNull(result.getBusiness().getBusinessDocuments().get(0)
                .getBusinessDocumentType().getId());
        Assert.assertNotNull(result.getBusiness().getBusinessDocuments().get(0)
                .getDocumentNumber());
        Assert.assertNotNull(result.getBusiness().getLegalName());
        Assert.assertNotNull(result.getLimitAmount().getAmount());
        Assert.assertNotNull(result.getLimitAmount().getCurrency());
        Assert.assertNotNull(result.getRelatedContracts().get(0).getContract()
                .getId());
        Assert.assertNotNull(result.getRelatedContracts().get(0).getProduct()
                .getId());
        Assert.assertNotNull(result.getRelatedContracts().get(0).getProduct()
                .getProductType().getId());
        Assert.assertNotNull(result.getRelatedContracts().get(0)
                .getRelationType().getId());
        Assert.assertNotNull(result.getProduct().getId());
        Assert.assertNotNull(result.getJoint().getId());
        Assert.assertNotNull(result.getBusinessManagers().get(0).getFirstName());
        Assert.assertNotNull(result.getBusinessManagers().get(0)
                .getMiddleName());
        Assert.assertNotNull(result.getBusinessManagers().get(0).getLastName());
        Assert.assertNotNull(result.getBusinessManagers().get(0)
                .getSecondLastName());
        Assert.assertNotNull(result.getBusinessManagers().get(0)
                .getIdentityDocuments().get(0).getDocumentType().getId());
        Assert.assertNotNull(result.getBusinessManagers().get(0)
                .getIdentityDocuments().get(0).getDocumentNumber());
        Assert.assertNotNull(result.getBusinessManagers().get(0)
                .getContactDetails().get(0).getContactType());
        Assert.assertNotNull(result.getBusinessManagers().get(0)
                .getContactDetails().get(0).getContact());
        Assert.assertNull(result.getBusinessManagers().get(0).getRoles());
        Assert.assertNotNull(result.getUnitManagement());
        Assert.assertNotNull(result.getBranch().getId());

        Assert.assertEquals(input.getBusiness().getId(), result.getBusiness()
                .getId());
        Assert.assertEquals(input.getBusiness()
                .getBusinessDocuments().get(0).getBusinessDocumentType()
                .getId(), result.getBusiness()
                .getBusinessDocuments().get(0).getBusinessDocumentType()
                .getId());
        Assert.assertEquals(input.getBusiness().getBusinessDocuments().get(0)
                .getDocumentNumber(), result.getBusiness()
                .getBusinessDocuments().get(0).getDocumentNumber());
        Assert.assertEquals(input.getBusiness().getLegalName(), result
                .getBusiness().getLegalName());
        Assert.assertEquals(input.getLimitAmount().getAmount(), result
                .getLimitAmount().getAmount());
        Assert.assertEquals(input.getLimitAmount().getCurrency(), result
                .getLimitAmount().getCurrency());
        Assert.assertEquals(input.getRelatedContracts().get(0).getContract()
                .getId(), result.getRelatedContracts().get(0).getContract()
                .getId());
        Assert.assertEquals(input.getRelatedContracts().get(0).getProduct()
                .getId(), result.getRelatedContracts().get(0).getProduct()
                .getId());
        Assert.assertEquals(input.getRelatedContracts().get(0)
                .getProduct().getProductType().getId(), result.getRelatedContracts().get(0)
                .getProduct().getProductType().getId());
        Assert.assertEquals(input.getRelatedContracts().get(0)
                .getRelationType().getId(), result.getRelatedContracts().get(0)
                .getRelationType().getId());
        Assert.assertEquals(input.getProduct()
                .getId(), result.getProduct()
                .getId());
        Assert.assertEquals(input.getJoint().getId(), result.getJoint().getId());
        Assert.assertEquals(input.getBusinessManagers().get(0).getFirstName(),
                result.getBusinessManagers().get(0).getFirstName());
        Assert.assertEquals(input.getBusinessManagers().get(0).getMiddleName(),
                result.getBusinessManagers().get(0).getMiddleName());
        Assert.assertEquals(input.getBusinessManagers().get(0).getLastName(),
                result.getBusinessManagers().get(0).getLastName());
        Assert.assertEquals(input.getBusinessManagers().get(0)
                .getSecondLastName(), result.getBusinessManagers().get(0)
                .getSecondLastName());
        Assert.assertEquals(input.getBusinessManagers().get(0).getIdentityDocuments()
                        .get(0).getDocumentType().getId(),
                result.getBusinessManagers().get(0).getIdentityDocuments()
                        .get(0).getDocumentType().getId());
        Assert.assertEquals(input.getBusinessManagers().get(0)
                .getIdentityDocuments().get(0).getDocumentNumber(), result
                .getBusinessManagers().get(0).getIdentityDocuments().get(0)
                .getDocumentNumber());
        Assert.assertEquals(input
                .getBusinessManagers().get(0).getContactDetails().get(0)
                .getContactType(), result
                .getBusinessManagers().get(0).getContactDetails().get(0)
                .getContactType());
        Assert.assertEquals(input.getBusinessManagers().get(0)
                .getContactDetails().get(0).getContact(), result
                .getBusinessManagers().get(0).getContactDetails().get(0)
                .getContact());

        Assert.assertEquals(input.getUnitManagement(),
                result.getUnitManagement());
        Assert.assertEquals(input.getBranch().getId(), result.getBranch()
                .getId());
    }

    @Test
    public void mapInEmptyTest() {
        ModelSubscriptionRequest result = mapper
                .mapIn(new SubscriptionRequest());
        Assert.assertNotNull(result);
        Assert.assertNull(result.getBusiness());
        Assert.assertNull(result.getLimitAmount());
        Assert.assertNull(result.getRelatedContracts());
        Assert.assertNull(result.getProduct());
        Assert.assertNull(result.getJoint());
        Assert.assertNull(result.getBusinessManagers());
        Assert.assertNull(result.getUnitManagement());
        Assert.assertNull(result.getBranch());
    }

    @Test
    public void mapOutFullTest() {
        ModelSubscriptionResponse response = ResponseFinancialManagementCompaniesMock.getInstance().buildModelSubscriptionResponse();

        String result = mapper.mapOut(response);
        Assert.assertNotNull(result);
    }

    @Test
    public void mapOutEmptyTest() {
        String result = mapper.mapOut(new ModelSubscriptionResponse());
        Assert.assertNull(result);

        result = mapper.mapOut(null);
        Assert.assertNull(result);
    }
}
