package com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.impl;

import com.bbva.pzic.financialmanagementcompanies.EntityStubs;
import com.bbva.pzic.financialmanagementcompanies.business.dto.InputFinancialManagementCompany;
import com.bbva.pzic.financialmanagementcompanies.business.dto.OutputFinancialManagementCompany;
import com.bbva.pzic.financialmanagementcompanies.dao.model.financialManagementCompany.ModelCreateFinancialManagementCompanyRequest;
import com.bbva.pzic.financialmanagementcompanies.dao.model.financialManagementCompany.ModelCreateFinancialManagementCompanyResponse;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import java.io.IOException;


public class RestCreateFinancialManagementCompanyMapperTest {

    @InjectMocks
    private RestCreateFinancialManagementCompanyMapper mapper = new RestCreateFinancialManagementCompanyMapper();

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void mapIn() throws IOException {

        InputFinancialManagementCompany input  =  EntityStubs.getInstance().getInputFinancialManagementCompany();

        ModelCreateFinancialManagementCompanyRequest requestDao = mapper.mapIn(input);

        Assert.assertEquals(requestDao.getNetcashType().getId(),input.getNetcashType().getId());
    }

    @Test
    public void mapOut() throws IOException {
        ModelCreateFinancialManagementCompanyResponse responseDao  =  EntityStubs.getInstance().getModelCreateFinancialManagementCompanyResponse();

        OutputFinancialManagementCompany output = mapper.mapOut(responseDao);

        Assert.assertEquals(output.getData().getId(),responseDao.getData().getId());
    }
}