package com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.financialmanagementcompanies.EntityStubs;
import com.bbva.pzic.financialmanagementcompanies.business.dto.DTOIntBusinessManager;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.BusinessManager;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.BusinessManagerId;
import com.bbva.pzic.financialmanagementcompanies.util.mappers.EnumMapper;
import com.bbva.pzic.utilTest.RandomGenerator;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

/**
 * Created on 18/09/2018.
 *
 * @author Entelgy
 */
public class CreateFinancialManagementCompaniesBusinessManagerMapperTest {

    private static final String BUSINESS_MANAGERS_IDENTITITY_DOCUMENT_DOCUMENT_TYPE_ID_BACKEND = "R";
    private static final String BUSINESS_MANAGERS_CONTACT_TYPE_ID_BACKEND = "F";
    private static final String BUSINESS_MANAGERS_ROLES_ID_BACKEND = "C";
    @InjectMocks
    private CreateFinancialManagementCompaniesBusinessManagerMapper mapper;

    @Mock
    private EnumMapper enumMapper;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void mapInFullTest() throws IOException {
        BusinessManager input = EntityStubs.getInstance().getBusinessManager();

        when(enumMapper.getBackendValue("businessManagers.identitityDocument.documentType.id",
                input.getIdentityDocument().getDocumentType().getId())).thenReturn(BUSINESS_MANAGERS_IDENTITITY_DOCUMENT_DOCUMENT_TYPE_ID_BACKEND);
        when(enumMapper.getBackendValue("businessManagers.contactType.id",
                input.getContactDetails().get(0).getContactType())).thenReturn(BUSINESS_MANAGERS_CONTACT_TYPE_ID_BACKEND);
        when(enumMapper.getBackendValue("businessManagers.contactType.id",
                input.getContactDetails().get(2).getContactType())).thenReturn(BUSINESS_MANAGERS_CONTACT_TYPE_ID_BACKEND);
        when(enumMapper.getBackendValue("businessManagers.roles.id",
                input.getRoles().get(0).getId())).thenReturn(BUSINESS_MANAGERS_ROLES_ID_BACKEND);
        when(enumMapper.getBackendValue("businessManagers.roles.id",
                input.getRoles().get(1).getId())).thenReturn(BUSINESS_MANAGERS_ROLES_ID_BACKEND);

        DTOIntBusinessManager result = mapper.mapIn(input);
        assertNotNull(result);
        assertNotNull(result.getFinancialManagementCompanyId());
        assertNotNull(result.getFullName());
        assertNotNull(result.getIdentityDocumentDocumentTypeId());
        assertNotNull(result.getIdentityDocumentDocumentNumber());
        assertNotNull(result.getContactDetailsContactType0());
        assertNotNull(result.getContactDetailsContact0());
        assertNull(result.getContactDetailsContactType1());
        assertNotNull(result.getContactDetailsContact1());
        assertNotNull(result.getContactDetailsContactType2());
        assertNull(result.getContactDetailsContact2());
        assertNull(result.getContactDetailsContactType3());
        assertNull(result.getContactDetailsContact3());
        assertNotNull(result.getRolesId0());
        assertNotNull(result.getRolesId1());
        assertNull(result.getRolesId2());
        assertEquals(input.getFinancialManagementCompanyId(), result.getFinancialManagementCompanyId());
        assertEquals(input.getFullName(), result.getFullName());
        assertEquals(BUSINESS_MANAGERS_IDENTITITY_DOCUMENT_DOCUMENT_TYPE_ID_BACKEND, result.getIdentityDocumentDocumentTypeId());
        assertEquals(input.getIdentityDocument().getDocumentNumber(), result.getIdentityDocumentDocumentNumber());
        assertEquals(BUSINESS_MANAGERS_CONTACT_TYPE_ID_BACKEND, result.getContactDetailsContactType0());
        assertEquals(input.getContactDetails().get(0).getContact(), result.getContactDetailsContact0());
        assertEquals(input.getContactDetails().get(1).getContact(), result.getContactDetailsContact1());
        assertEquals(BUSINESS_MANAGERS_CONTACT_TYPE_ID_BACKEND, result.getContactDetailsContactType2());
        assertEquals(BUSINESS_MANAGERS_ROLES_ID_BACKEND, result.getRolesId0());
        assertEquals(BUSINESS_MANAGERS_ROLES_ID_BACKEND, result.getRolesId1());
    }

    @Test
    public void mapInEmptyTest() {
        DTOIntBusinessManager result = mapper.mapIn(new BusinessManager());
        assertNotNull(result);
        assertNull(result.getFinancialManagementCompanyId());
        assertNull(result.getFullName());
        assertNull(result.getIdentityDocumentDocumentTypeId());
        assertNull(result.getIdentityDocumentDocumentNumber());
        assertNull(result.getContactDetailsContactType0());
        assertNull(result.getContactDetailsContact0());
        assertNull(result.getContactDetailsContactType1());
        assertNull(result.getContactDetailsContact1());
        assertNull(result.getContactDetailsContactType2());
        assertNull(result.getContactDetailsContact2());
        assertNull(result.getContactDetailsContactType3());
        assertNull(result.getContactDetailsContact3());
        assertNull(result.getRolesId0());
        assertNull(result.getRolesId1());
        assertNull(result.getRolesId2());
    }

    @Test
    public void mapOutFullTest() {
        ServiceResponse<BusinessManagerId> result = mapper.mapOut(RandomGenerator.getString(24));
        assertNotNull(result);
        assertNotNull(result.getData());
    }

    @Test
    public void mapOutEmptyTest() {
        ServiceResponse<BusinessManagerId> result = mapper.mapOut(null);
        assertNull(result);
    }
}
