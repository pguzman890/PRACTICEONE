package com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.financialmanagementcompanies.EntityStubs;
import com.bbva.pzic.financialmanagementcompanies.business.dto.InputGetFMCAuthorizedBusinessManagerProfiledService;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.ProfiledService;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper.IGetFMCAuthorizedBusinessManagerProfiledServiceMapper;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created on 04/03/2020.
 *
 * @author Entelgy
 */
public class GetFMCAuthorizedBusinessManagerProfiledServiceMapperTest {

    private IGetFMCAuthorizedBusinessManagerProfiledServiceMapper mapper;

    @Before
    public void setUp() {
        mapper = new GetFMCAuthorizedBusinessManagerProfiledServiceMapper();
    }

    @Test
    public void mapInFullTest() {
        InputGetFMCAuthorizedBusinessManagerProfiledService result = mapper.mapIn(
                EntityStubs.FINANCIAL_MANAGEMENT_COMPANY_ID,
                EntityStubs.AUTHORIZED_BUSINESS_MANAGER_ID,
                EntityStubs.PROFILED_SERVICE_ID);

        assertNotNull(result);
        assertNotNull(result.getFinancialManagementCompanyId());
        assertNotNull(result.getAuthorizedBusinessManagerId());
        assertNotNull(result.getProfiledServiceId());

        assertEquals(EntityStubs.FINANCIAL_MANAGEMENT_COMPANY_ID, result.getFinancialManagementCompanyId());
        assertEquals(EntityStubs.AUTHORIZED_BUSINESS_MANAGER_ID, result.getAuthorizedBusinessManagerId());
        assertEquals(EntityStubs.PROFILED_SERVICE_ID, result.getProfiledServiceId());
    }

    @Test
    public void mapOutFullTest() {
        ServiceResponse<ProfiledService> result = mapper.mapOut(new ProfiledService());
        assertNotNull(result);
        assertNotNull(result.getData());
    }

    @Test
    public void mapOutEmptyTest() {
        ServiceResponse<ProfiledService> result = mapper.mapOut(null);
        assertNull(result);
    }
}
