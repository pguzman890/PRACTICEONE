package com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.financialmanagementcompanies.EntityStubs;
import com.bbva.pzic.financialmanagementcompanies.business.dto.InputFinancialManagementCompany;
import com.bbva.pzic.financialmanagementcompanies.business.dto.OutputFinancialManagementCompany;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.CreateFinancialManagementCompanyRequest;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.CreateFinancialManagementCompanyResponse;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import java.io.IOException;


public class CreateFinancialManagementCompanyReqResMapperTest {

    @InjectMocks
    private CreateFinancialManagementCompanyReqResMapper mapper;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void mapInOk() throws IOException {

        CreateFinancialManagementCompanyRequest request = EntityStubs.getInstance().getCreateFinancialManagementCompanyRequest();

        InputFinancialManagementCompany input = mapper.mapIn(request);

        Assert.assertEquals(request.getBusiness().getLimitAmount().getAmount(),input.getBusiness().getLimitAmount().getAmount());
    }

    @Test
    public void mapOutOk() throws IOException {

        OutputFinancialManagementCompany output = EntityStubs.getInstance().getOutputFinancialManagementCompany();

        ServiceResponse<CreateFinancialManagementCompanyResponse> reponse = mapper.mapOut(output);

        Assert.assertEquals(reponse.getData().getData().getId(),output.getData().getId());
    }
}