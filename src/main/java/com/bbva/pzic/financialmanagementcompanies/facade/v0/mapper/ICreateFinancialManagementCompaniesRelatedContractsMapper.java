package com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper;

import com.bbva.pzic.financialmanagementcompanies.business.dto.InputCreateFinancialManagementCompaniesRelatedContracts;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.RelatedContract;

import java.util.List;

/**
 * Created on 13/09/2018.
 *
 * @author Entelgy
 */
public interface ICreateFinancialManagementCompaniesRelatedContractsMapper {

    InputCreateFinancialManagementCompaniesRelatedContracts mapIn(
            String financialManagementCompanyId,
            List<RelatedContract> relatedContract);
}
