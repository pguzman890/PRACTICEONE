package com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper.common;

import com.bbva.pzic.financialmanagementcompanies.business.dto.DTOIntRequestBody;
import com.bbva.pzic.financialmanagementcompanies.business.dto.DTOIntRequestHeader;

public interface IPasswordMapper {

    DTOIntRequestHeader mapHeader();

    DTOIntRequestBody mapBody(String aap);

    String mapLdapField(String aap, String key);
}
