package com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.financialmanagementcompanies.business.dto.InputFinancialManagementCompany;
import com.bbva.pzic.financialmanagementcompanies.business.dto.OutputFinancialManagementCompany;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.*;

public interface ICreateFinancialManagementCompanyReqResMapper {

    InputFinancialManagementCompany mapIn(CreateFinancialManagementCompanyRequest createFinancialManagementCompanyRequest);

    ServiceResponse<CreateFinancialManagementCompanyResponse> mapOut(OutputFinancialManagementCompany createFinancialManagementCompanyRequest);
}
