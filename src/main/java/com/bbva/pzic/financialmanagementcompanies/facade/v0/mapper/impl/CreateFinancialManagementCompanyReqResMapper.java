package com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.financialmanagementcompanies.business.dto.InputFinancialManagementCompany;
import com.bbva.pzic.financialmanagementcompanies.business.dto.OutputFinancialManagementCompany;
import com.bbva.pzic.financialmanagementcompanies.dao.model.financialManagementCompany.*;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.CreateFinancialManagementCompanyRequest;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.CreateFinancialManagementCompanyResponse;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.Data;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper.ICreateFinancialManagementCompanyReqResMapper;
import com.bbva.pzic.financialmanagementcompanies.util.mappers.Mapper;
import com.bbva.pzic.financialmanagementcompanies.util.orika.impl.ConfigurableMapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.List;

@Mapper
public class CreateFinancialManagementCompanyReqResMapper extends ConfigurableMapper
        implements ICreateFinancialManagementCompanyReqResMapper {

    private static final Log LOG = LogFactory
            .getLog(CreateFinancialManagementCompanyReqResMapper.class);

    @Override
    public InputFinancialManagementCompany mapIn(CreateFinancialManagementCompanyRequest createFinancialManagementCompanyRequest) {
        LOG.info("... called method CreateFinancialManagementCompanyReqResMapper.mapIn ...");
        InputFinancialManagementCompany inputFinancialManagementCompany  = new InputFinancialManagementCompany();

        map(createFinancialManagementCompanyRequest, inputFinancialManagementCompany);

        return inputFinancialManagementCompany;
    }

    @Override
    public ServiceResponse<CreateFinancialManagementCompanyResponse> mapOut(OutputFinancialManagementCompany outputFinancialManagementCompany) {
        LOG.info("... called method CreateFinancialManagementCompanyReqResMapper.mapIn ...");

        CreateFinancialManagementCompanyResponse response = new CreateFinancialManagementCompanyResponse();

        response.setData(new Data());
        response.getData().setId(outputFinancialManagementCompany.getData().getId());

        return ServiceResponse.data(response).build();
    }

    protected void map( CreateFinancialManagementCompanyRequest createFinancialManagementCompanyRequest, InputFinancialManagementCompany inputFinancialManagementCompany) {

        Business business= new Business();

        List<BusinessDocument> businessDocuments= new ArrayList<BusinessDocument>();

        business.setBusinessDocuments(businessDocuments);

        BusinessManagement businessManagement= new BusinessManagement();
        ManagementType managementType = new ManagementType();
        managementType.setId(createFinancialManagementCompanyRequest.getBusiness().getBusinessManagement().getManagementType().getId());
        businessManagement.setManagementType( managementType );
        business.setBusinessManagement( businessManagement );

        LimitAmount limitAmount = new LimitAmount();
        limitAmount.setAmount(createFinancialManagementCompanyRequest.getBusiness().getLimitAmount().getAmount());
        limitAmount.setCurrency(createFinancialManagementCompanyRequest.getBusiness().getLimitAmount().getCurrency());
        business.setLimitAmount(limitAmount);
        inputFinancialManagementCompany.setBusiness(business);

        Contract contract= new Contract();
        contract.setId(createFinancialManagementCompanyRequest.getContract().getId());
        inputFinancialManagementCompany.setContract(contract);

        NetcashType netcashType = new NetcashType();
        netcashType.setId(createFinancialManagementCompanyRequest.getNetcashType().getId());
        Version version = new Version();
        version.setId(createFinancialManagementCompanyRequest.getNetcashType().getVersion().getId());
        netcashType.setVersion(version);

        inputFinancialManagementCompany.setNetcashType(netcashType);

        Product product= new Product();
        product.setId(createFinancialManagementCompanyRequest.getProduct().getId());
        ProductType productType = new ProductType();
        productType.setId(createFinancialManagementCompanyRequest.getProduct().getProductType().getId());
        product.setProductType(productType);
        inputFinancialManagementCompany.setProduct(product);

        List<ReviewerNetcash> reviewers = new ArrayList<ReviewerNetcash>();

        for (com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.ReviewerNetcash reviewerNetcash:
                createFinancialManagementCompanyRequest.getReviewers()
             ) {
            reviewers.add(new ReviewerNetcash(){{

                this.setBusinessAgentId(reviewerNetcash.getBusinessAgentId());
                this.setUnitManagement(reviewerNetcash.getUnitManagement());
                this.setRegistrationIdentifier(reviewerNetcash.getRegistrationIdentifier());
                this.setProfessionPosition(reviewerNetcash.getProfessionPosition());

                this.setBank(new Bank(){{
                    this.setId(reviewerNetcash.getBank().getId());
                    this.setBranch(
                            new Branch(){{
                              this.setId(reviewerNetcash.getBank().getBranch().getId());
                            }}
                    );
                }});

                this.setProfile(new Profile(){{
                    this.setId(reviewerNetcash.getProfile().getId());
                }});

                List<ContactDetail> contactDetails= new ArrayList<ContactDetail>();

                for (com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.ContactDetail contactDetail:
                        reviewerNetcash.getContactDetails()) {
                    contactDetails.add(new ContactDetail(){{
                        this.setContact(contactDetail.getContact());
                        this.setContactType(contactDetail.getContactType());
                    }});
                }

                this.setContactDetails(contactDetails);

            }});
        }

        inputFinancialManagementCompany.setReviewers(reviewers);

        RelationType relationType= new RelationType();
        relationType.setId(createFinancialManagementCompanyRequest.getRelationType().getId());
        inputFinancialManagementCompany.setRelationType(relationType);
    }
}
