package com.bbva.pzic.financialmanagementcompanies.facade.v0.dto;

import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "serviceContract", namespace = "urn:com:bbva:pzic:financialmanagementcompanies:facade:v0:dto")
@XmlType(name = "serviceContract", namespace = "urn:com:bbva:pzic:financialmanagementcompanies:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class ServiceContract implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Service identifier.
     */
    @DatoAuditable(omitir = true)
    private String id;
    /**
     * Service description.
     */
    private String name;
    /**
     * Contract operative type.
     */
    private String contractType;
    /**
     * Contract number.
     */
    @DatoAuditable(omitir = true)
    private String number;
    /**
     * Type of contract number.
     */
    private String numberType;
    /**
     * Favourite indicator. If it is true, the contract will be seen at home page.
     */
    private Boolean isFavourite;
    /**
     * Contract alias for the authorized business manager.
     */
    private String alias;
    /**
     * Contract currency. String based on ISO-4217 for specifying
     * the currency related to the current amount.
     */
    private String currency;
    /**
     * Contract country.
     */
    private Country country;
    /**
     * Contract bank.
     */
    private Bank bank;
    /**
     * Internal unique contract identifier.
     */
    @DatoAuditable(omitir = true)
    private String internalContractNumber;
    /**
     * Contract counterparty.
     */
    @DatoAuditable(omitir = true)
    private String counterparty;
    /**
     * Criteria that administrators will have when performing operations over services.
     * This configuration establishes the permissions for a specific contract.
     * For all those contracts that are not configured here, they will inherit
     * the permission configuration at the level of the associated service.
     * In case the service of this contract is not configured will inherit
     * the configuration permissions at manager level.
     */
    private OperationRights operationRights;
    /**
     * It represents the information about contract holder.
     */
    private Holder holder;
    /**
     * Information about amount of balance of the contract.
     */
    private Balance balance;
    /**
     * Title of the financial product associated with the contract.
     */
    private Product product;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContractType() {
        return contractType;
    }

    public void setContractType(String contractType) {
        this.contractType = contractType;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getNumberType() {
        return numberType;
    }

    public void setNumberType(String numberType) {
        this.numberType = numberType;
    }

    public Boolean getIsFavourite() {
        return isFavourite;
    }

    public void setIsFavourite(Boolean isFavourite) {
        this.isFavourite = isFavourite;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public Bank getBank() {
        return bank;
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }

    public String getInternalContractNumber() {
        return internalContractNumber;
    }

    public void setInternalContractNumber(String internalContractNumber) {
        this.internalContractNumber = internalContractNumber;
    }

    public String getCounterparty() {
        return counterparty;
    }

    public void setCounterparty(String counterparty) {
        this.counterparty = counterparty;
    }

    public OperationRights getOperationRights() {
        return operationRights;
    }

    public void setOperationRights(OperationRights operationRights) {
        this.operationRights = operationRights;
    }

    public Holder getHolder() {
        return holder;
    }

    public void setHolder(Holder holder) {
        this.holder = holder;
    }

    public Balance getBalance() {
        return balance;
    }

    public void setBalance(Balance balance) {
        this.balance = balance;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
}
