package com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.financialmanagementcompanies.business.dto.InputGetSubscriptionRequest;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.SubscriptionRequest;

/**
 * Created on 03/08/2018.
 *
 * @author Entelgy
 */
public interface IGetSubscriptionRequestMapper {

    InputGetSubscriptionRequest mapIn(String subscriptionRequestId);

    ServiceResponse<SubscriptionRequest> mapOut(SubscriptionRequest data);
}
