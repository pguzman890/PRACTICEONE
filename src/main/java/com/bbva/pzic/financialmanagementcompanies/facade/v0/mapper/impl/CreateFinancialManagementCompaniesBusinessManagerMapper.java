package com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.financialmanagementcompanies.business.dto.DTOIntBusinessManager;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.BusinessManager;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.BusinessManagerId;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper.ICreateFinancialManagementCompaniesBusinessManagerMapper;
import com.bbva.pzic.financialmanagementcompanies.util.mappers.EnumMapper;
import com.bbva.pzic.financialmanagementcompanies.util.mappers.Mapper;
import com.bbva.pzic.financialmanagementcompanies.util.orika.MapperFactory;
import com.bbva.pzic.financialmanagementcompanies.util.orika.impl.ConfigurableMapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created on 18/09/2018.
 *
 * @author Entelgy
 */
@Mapper
public class CreateFinancialManagementCompaniesBusinessManagerMapper extends ConfigurableMapper implements ICreateFinancialManagementCompaniesBusinessManagerMapper {

    private static final Log LOG = LogFactory.getLog(CreateFinancialManagementCompaniesBusinessManagerMapper.class);
    private static final String BUSINESS_MANAGERS_CONTACT_TYPE_ID_FIELD = "businessManagers.contactType.id";
    private static final String BUSINESS_MANAGERS_ROLES_ID_FIELD = "businessManagers.roles.id";

    @Autowired
    private EnumMapper enumMapper;

    @Override
    protected void configure(MapperFactory factory) {
        super.configure(factory);
        factory.classMap(BusinessManager.class, DTOIntBusinessManager.class)
                .field("financialManagementCompanyId", "financialManagementCompanyId")
                .field("fullName", "fullName")
                .field("identityDocument.documentType.id", "identityDocumentDocumentTypeId")
                .field("identityDocument.documentNumber", "identityDocumentDocumentNumber")
                .field("contactDetails[0].contactType", "contactDetailsContactType0")
                .field("contactDetails[0].contact", "contactDetailsContact0")
                .field("contactDetails[1].contactType", "contactDetailsContactType1")
                .field("contactDetails[1].contact", "contactDetailsContact1")
                .field("contactDetails[2].contactType", "contactDetailsContactType2")
                .field("contactDetails[2].contact", "contactDetailsContact2")
                .field("contactDetails[3].contactType", "contactDetailsContactType3")
                .field("contactDetails[3].contact", "contactDetailsContact3")
                .field("roles[0].id", "rolesId0")
                .field("roles[1].id", "rolesId1")
                .field("roles[2].id", "rolesId2")
                .register();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DTOIntBusinessManager mapIn(final BusinessManager businessManager) {
        LOG.info("... called method CreateFinancialManagementCompaniesBusinessManagerMapper.mapIn ...");
        DTOIntBusinessManager dtoInt = map(businessManager, DTOIntBusinessManager.class);
        dtoInt.setIdentityDocumentDocumentTypeId(enumMapper.getBackendValue("businessManagers.identitityDocument.documentType.id", dtoInt.getIdentityDocumentDocumentTypeId()));
        dtoInt.setContactDetailsContactType0(enumMapper.getBackendValue(BUSINESS_MANAGERS_CONTACT_TYPE_ID_FIELD, dtoInt.getContactDetailsContactType0()));
        dtoInt.setContactDetailsContactType1(enumMapper.getBackendValue(BUSINESS_MANAGERS_CONTACT_TYPE_ID_FIELD, dtoInt.getContactDetailsContactType1()));
        dtoInt.setContactDetailsContactType2(enumMapper.getBackendValue(BUSINESS_MANAGERS_CONTACT_TYPE_ID_FIELD, dtoInt.getContactDetailsContactType2()));
        dtoInt.setContactDetailsContactType3(enumMapper.getBackendValue(BUSINESS_MANAGERS_CONTACT_TYPE_ID_FIELD, dtoInt.getContactDetailsContactType3()));
        dtoInt.setRolesId0(enumMapper.getBackendValue(BUSINESS_MANAGERS_ROLES_ID_FIELD, dtoInt.getRolesId0()));
        dtoInt.setRolesId1(enumMapper.getBackendValue(BUSINESS_MANAGERS_ROLES_ID_FIELD, dtoInt.getRolesId1()));
        dtoInt.setRolesId2(enumMapper.getBackendValue(BUSINESS_MANAGERS_ROLES_ID_FIELD, dtoInt.getRolesId2()));
        return dtoInt;
    }

    @SuppressWarnings("unchecked")
    @Override
    public ServiceResponse<BusinessManagerId> mapOut(String identifier) {
        LOG.info("... called method CreateFinancialManagementCompaniesBusinessManagerMapper.mapOut ...");
        if (identifier == null) {
            return null;
        }
        BusinessManagerId requestId = new BusinessManagerId();
        requestId.setId(identifier);
        return ServiceResponse.data(requestId).build();
    }
}
