package com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper.impl;

import com.bbva.pzic.financialmanagementcompanies.business.dto.InputModifySubscriptionRequest;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.SubscriptionRequest;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper.IModifySubscriptionRequestMapper;
import com.bbva.pzic.financialmanagementcompanies.util.mappers.EnumMapper;
import com.bbva.pzic.financialmanagementcompanies.util.mappers.Mapper;
import com.bbva.pzic.financialmanagementcompanies.util.orika.MapperFactory;
import com.bbva.pzic.financialmanagementcompanies.util.orika.impl.ConfigurableMapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created on 03/08/2018.
 *
 * @author Entelgy
 */
@Mapper
public class ModifySubscriptionRequestMapper extends ConfigurableMapper
        implements
        IModifySubscriptionRequestMapper {

    private static final Log LOG = LogFactory
            .getLog(ModifySubscriptionRequestMapper.class);

    @Autowired
    private EnumMapper enumMapper;

    /**
     * {@inheritDoc}
     */
    @Override
    public InputModifySubscriptionRequest mapIn(
            final String subscriptionRequestId,
            final SubscriptionRequest subscriptionRequest) {
        LOG.info("... called method ModifySubscriptionRequestMapper.mapIn ...");
        InputModifySubscriptionRequest input = new InputModifySubscriptionRequest();
        input.setSubscriptionRequestId(subscriptionRequestId);
        input.setSubscriptionRequest(subscriptionRequest);

        if (subscriptionRequest.getStatus() != null) {
            subscriptionRequest.getStatus().setId(enumMapper.getBackendValue("suscriptionRequest.status.id", subscriptionRequest.getStatus().getId()));
            subscriptionRequest.getStatus().setReason(enumMapper.getBackendValue("suscriptionRequest.status.reason.id", subscriptionRequest.getStatus().getReason()));
        }

        return input;
    }

    @Override
    protected void configure(MapperFactory factory) {
        super.configure(factory);
        factory.classMap(SubscriptionRequest.class,
                InputModifySubscriptionRequest.class)
                .field("status.id", "subscriptionRequest.status.id")
                .field("status.reason", "subscriptionRequest.status.reason")
                .field("comment", "subscriptionRequest.comment").register();
    }
}
