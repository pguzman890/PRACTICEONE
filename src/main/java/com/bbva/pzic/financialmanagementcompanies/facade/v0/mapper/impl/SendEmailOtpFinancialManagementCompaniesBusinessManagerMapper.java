package com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper.impl;

import com.bbva.pzic.financialmanagementcompanies.business.dto.DTOIntRequestBody;
import com.bbva.pzic.financialmanagementcompanies.business.dto.DTOIntRequestHeader;
import com.bbva.pzic.financialmanagementcompanies.business.dto.InputRequestBackendRest;
import com.bbva.pzic.financialmanagementcompanies.business.dto.InputSendEmailOtpFinancialManagementCompaniesBusinessManager;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper.ISendEmailOtpFinancialManagementCompaniesBusinessManagerMapper;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper.common.IPasswordMapper;
import com.bbva.pzic.financialmanagementcompanies.routine.PasswordGenerator;
import com.bbva.pzic.financialmanagementcompanies.util.mappers.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper
public class SendEmailOtpFinancialManagementCompaniesBusinessManagerMapper implements ISendEmailOtpFinancialManagementCompaniesBusinessManagerMapper {


    @Autowired
    private IPasswordMapper passwordMapper;

    @Autowired
    private PasswordGenerator passwordGenerator;

    @Override
    public InputRequestBackendRest mapIn(final String businessManagerId) {
        InputRequestBackendRest input = new InputRequestBackendRest();
        input.setHeader(passwordMapper.mapHeader());

        final String aap = input.getHeader().getAap();
        input.setAap(aap);

        DTOIntRequestBody requestBody = passwordMapper.mapBody(aap);
        input.setRequestBody(requestBody);
        input.getRequestBody().setDataOperationAlias(businessManagerId);
        input.getRequestBody().setDataOperationPassword(passwordGenerator.generatePassword(aap));
        return input;
    }

    private DTOIntRequestHeader getHeader(DTOIntRequestHeader requestHeader) {
        DTOIntRequestHeader header = new DTOIntRequestHeader();
        header.setUser(requestHeader.getUser());
        header.setContactID(requestHeader.getContactID());
        header.setRequestID(requestHeader.getRequestID());
        header.setAap(requestHeader.getAap());
        header.setServiceID(requestHeader.getServiceID());
        return header;
    }

    private DTOIntRequestBody getRequestBody(DTOIntRequestBody requestBody) {
        DTOIntRequestBody body = new DTOIntRequestBody();
        body.setDataOperationPdgroup(requestBody.getDataOperationPdgroup());
        body.setDataOperationBank(requestBody.getDataOperationBank());
        body.setCountry(requestBody.getCountry());
        body.setDataOptionalsGrupo(requestBody.getDataOptionalsGrupo());
        body.setDataOptionalsLdap(requestBody.getDataOptionalsLdap());
        body.setDataOptionalsPrefijo(requestBody.getDataOptionalsPrefijo());
        body.setDataOptionalsRama(requestBody.getDataOptionalsRama());
        body.setOperation(requestBody.getOperation());
        body.setDataOperationAlias(requestBody.getDataOperationAlias());
        return body;
    }

    private InputRequestBackendRest mapDefault(final InputRequestBackendRest input) {
        InputRequestBackendRest mapIn = new InputRequestBackendRest();
        mapIn.setHeader(getHeader(input.getHeader()));
        mapIn.setRequestBody(getRequestBody(input.getRequestBody()));
        mapIn.getRequestBody().setDataOperationAlias(input.getRequestBody().getDataOperationAlias());
        return mapIn;
    }

    @Override
    public InputRequestBackendRest mapInReactivate(InputRequestBackendRest input) {
        InputRequestBackendRest mapIn = mapDefault(input);
        final String aap = mapIn.getHeader().getAap();
        mapIn.getRequestBody().setOperation(passwordMapper.mapLdapField(aap, "smc.configuration.SMCPE1810337.ldapNetcash.reactivateUserSimplePwdChg"));
        mapIn.getRequestBody().setDataOperationPdgroup(null);
        mapIn.getRequestBody().setDataOperationNewpassword(input.getRequestBody().getDataOperationPassword());
        return mapIn;
    }

    @Override
    public InputRequestBackendRest mapInDelUser(InputRequestBackendRest input) {
        InputRequestBackendRest mapIn = mapDefault(input);
        final String aap = mapIn.getHeader().getAap();
        mapIn.getRequestBody().setOperation(passwordMapper.mapLdapField(aap, "smc.configuration.SMCPE1810337.ldapNetcash.delUser"));
        mapIn.getRequestBody().setDataOperationNewpassword(null);
        mapIn.getRequestBody().setDataOperationPdgroup(null);
        return mapIn;
    }

    @Override
    public InputRequestBackendRest mapInDelFromGroupUser(InputRequestBackendRest input) {
        InputRequestBackendRest mapIn = mapDefault(input);
        final String aap = mapIn.getHeader().getAap();
        mapIn.getRequestBody().setOperation(passwordMapper.mapLdapField(aap, "smc.configuration.SMCPE1810337.ldapNetcash.delUserFromGroup"));
        mapIn.getRequestBody().setDataOperationNewpassword(null);
        mapIn.getRequestBody().setDataOperationBank(null);
        return mapIn;
    }

    @Override
    public InputSendEmailOtpFinancialManagementCompaniesBusinessManager mapInGetBusinessManagerData(String businessManagerId, InputRequestBackendRest input) {
        InputSendEmailOtpFinancialManagementCompaniesBusinessManager mapIn = new InputSendEmailOtpFinancialManagementCompaniesBusinessManager();
        mapIn.setBusinessManagerId(businessManagerId);
        mapIn.setPassword(input.getRequestBody().getDataOperationPassword());
        return mapIn;
    }

    @Override
    public InputSendEmailOtpFinancialManagementCompaniesBusinessManager mapInSendEmail(String businessManagerId, InputRequestBackendRest inputRequestBackendRest, InputSendEmailOtpFinancialManagementCompaniesBusinessManager input) {
        InputSendEmailOtpFinancialManagementCompaniesBusinessManager mapper = new InputSendEmailOtpFinancialManagementCompaniesBusinessManager();
        if (input != null) {
            mapper.setBusinessManagerId(businessManagerId);
            mapper.setPassword(inputRequestBackendRest.getRequestBody().getDataOperationPassword());
            mapper.setEmailList(input.getEmailList());
            mapper.setBusinessName(input.getBusinessName());
            mapper.setUserName(input.getUserName());
        }
        return mapper;
    }
}
