package com.bbva.pzic.financialmanagementcompanies.facade.v0.dto;

public class Data {
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
