package com.bbva.pzic.financialmanagementcompanies.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "validateOperationFeasibility", namespace = "urn:com:bbva:pzic:financialmanagementcompanies:facade:v0:dto")
@XmlType(name = "validateOperationFeasibility", namespace = "urn:com:bbva:pzic:financialmanagementcompanies:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class ValidateOperationFeasibility implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * It represents information about contract selected by company to perform a
     * operation.
     */
    private ContractValidate contract;
    /**
     * It represents information about amount of operation that company needs to
     * perform.
     */
    private Amount operationAmount;
    /**
     * It represents information about process to evaluation of request company
     * to perform an operation.
     */
    private Result result;

    public ContractValidate getContract() {
        return contract;
    }

    public void setContract(ContractValidate contract) {
        this.contract = contract;
    }

    public Amount getOperationAmount() {
        return operationAmount;
    }

    public void setOperationAmount(Amount operationAmount) {
        this.operationAmount = operationAmount;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }
}
