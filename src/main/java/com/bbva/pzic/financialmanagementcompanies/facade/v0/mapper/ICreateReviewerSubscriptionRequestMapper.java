package com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.financialmanagementcompanies.business.dto.InputCreateReviewerSubscriptionRequest;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.Reviewer;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.ReviewerId;

/**
 * Created on 14/08/2018.
 *
 * @author Entelgy
 */
public interface ICreateReviewerSubscriptionRequestMapper {

    InputCreateReviewerSubscriptionRequest mapIn(String subscriptionRequestId, Reviewer reviewer);

    ServiceResponse<ReviewerId> mapOut(String identifier);
}
