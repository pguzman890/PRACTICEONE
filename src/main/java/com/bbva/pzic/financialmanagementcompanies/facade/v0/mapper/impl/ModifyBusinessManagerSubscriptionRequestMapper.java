package com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper.impl;

import com.bbva.pzic.financialmanagementcompanies.business.dto.InputModifyBusinessManagerSubscriptionRequest;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.BusinessManagerSubscriptionRequest;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper.IModifyBusinessManagerSubscriptionRequestMapper;
import com.bbva.pzic.financialmanagementcompanies.util.mappers.Mapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created on 14/08/2018.
 *
 * @author Entelgy
 */
@Mapper
public class ModifyBusinessManagerSubscriptionRequestMapper implements IModifyBusinessManagerSubscriptionRequestMapper {

    private static final Log LOG = LogFactory.getLog(ModifyBusinessManagerSubscriptionRequestMapper.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public InputModifyBusinessManagerSubscriptionRequest mapIn(final String subscriptionRequestId, final String businessManagerId,
                                                               final BusinessManagerSubscriptionRequest businessManagerSubscriptionRequest) {
        LOG.info("... called method ModifyBusinessManagerSubscriptionRequestMapper.mapIn ...");
        InputModifyBusinessManagerSubscriptionRequest input = new InputModifyBusinessManagerSubscriptionRequest();
        input.setSubscriptionRequestId(subscriptionRequestId);
        input.setBusinessManagerId(businessManagerId);
        input.setBusinessManagerSubscriptionRequest(businessManagerSubscriptionRequest);
        return input;
    }
}
