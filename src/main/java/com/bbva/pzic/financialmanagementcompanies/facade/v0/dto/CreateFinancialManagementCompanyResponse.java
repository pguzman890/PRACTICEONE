package com.bbva.pzic.financialmanagementcompanies.facade.v0.dto;


public class CreateFinancialManagementCompanyResponse {

    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
