package com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper;

import com.bbva.pzic.financialmanagementcompanies.business.dto.InputRequestBackendRest;
import com.bbva.pzic.financialmanagementcompanies.business.dto.InputSendEmailOtpFinancialManagementCompaniesBusinessManager;

public interface ISendEmailOtpFinancialManagementCompaniesBusinessManagerMapper {

    InputRequestBackendRest mapIn(String businessManagerId);

    InputRequestBackendRest mapInReactivate(InputRequestBackendRest input);

    InputRequestBackendRest mapInDelUser(InputRequestBackendRest input);

    InputRequestBackendRest mapInDelFromGroupUser(InputRequestBackendRest input);

    InputSendEmailOtpFinancialManagementCompaniesBusinessManager mapInGetBusinessManagerData(String businessManagerId, InputRequestBackendRest input);

    InputSendEmailOtpFinancialManagementCompaniesBusinessManager mapInSendEmail(String businessManagerId, InputRequestBackendRest inputRequestBackendRest, InputSendEmailOtpFinancialManagementCompaniesBusinessManager input);
}
