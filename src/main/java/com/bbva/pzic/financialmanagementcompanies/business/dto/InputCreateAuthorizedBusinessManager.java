package com.bbva.pzic.financialmanagementcompanies.business.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created on 18/09/2018.
 *
 * @author Entelgy
 */
public class InputCreateAuthorizedBusinessManager {

    @Size(max = 8, groups = ValidationGroup.CreateFinancialManagementCompaniesAuthorizedBusinessManager.class)
    private String financialManagementCompanyId;

    @NotNull(groups = ValidationGroup.CreateFinancialManagementCompaniesAuthorizedBusinessManager.class)
    @Size(max = 24, groups = ValidationGroup.CreateFinancialManagementCompaniesAuthorizedBusinessManager.class)
    private String businessManagerId;

    @NotNull(groups = ValidationGroup.CreateFinancialManagementCompaniesAuthorizedBusinessManager.class)
    @Size(max = 1, groups = ValidationGroup.CreateFinancialManagementCompaniesAuthorizedBusinessManager.class)
    private String validationRightsId;
    @NotNull(groups = ValidationGroup.CreateFinancialManagementCompaniesAuthorizedBusinessManager.class)
    @Size(max = 1, groups = ValidationGroup.CreateFinancialManagementCompaniesAuthorizedBusinessManager.class)
    private String permissionTypeId;

    @NotNull(groups = ValidationGroup.CreateFinancialManagementCompaniesAuthorizedBusinessManager.class)
    @Size(max = 2, groups = ValidationGroup.CreateFinancialManagementCompaniesAuthorizedBusinessManager.class)
    private String businessManagementStatus;

    public String getFinancialManagementCompanyId() {
        return financialManagementCompanyId;
    }

    public void setFinancialManagementCompanyId(String financialManagementCompanyId) {
        this.financialManagementCompanyId = financialManagementCompanyId;
    }

    public String getBusinessManagerId() {
        return businessManagerId;
    }

    public void setBusinessManagerId(String businessManagerId) {
        this.businessManagerId = businessManagerId;
    }

    public String getValidationRightsId() {
        return validationRightsId;
    }

    public void setValidationRightsId(String validationRightsId) {
        this.validationRightsId = validationRightsId;
    }

    public String getPermissionTypeId() {
        return permissionTypeId;
    }

    public void setPermissionTypeId(String permissionTypeId) {
        this.permissionTypeId = permissionTypeId;
    }

    public String getBusinessManagementStatus() {
        return businessManagementStatus;
    }

    public void setBusinessManagementStatus(String businessManagementStatus) {
        this.businessManagementStatus = businessManagementStatus;
    }

}