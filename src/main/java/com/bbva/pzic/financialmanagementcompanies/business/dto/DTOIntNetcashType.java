package com.bbva.pzic.financialmanagementcompanies.business.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created on 15/08/2018.
 *
 * @author Entelgy
 */
public class DTOIntNetcashType {

    @NotNull(groups = ValidationGroup.CreateFinancialManagementCompany.class)
    private String id;
    @Valid
    @NotNull(groups = ValidationGroup.CreateFinancialManagementCompany.class)
    private DTOIntVersionProduct version;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public DTOIntVersionProduct getVersion() {
        return version;
    }

    public void setVersion(DTOIntVersionProduct version) {
        this.version = version;
    }
}