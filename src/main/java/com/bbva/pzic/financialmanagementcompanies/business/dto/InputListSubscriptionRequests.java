package com.bbva.pzic.financialmanagementcompanies.business.dto;


/**
 * Created on 03/08/2018.
 *
 * @author Entelgy
 */
public class InputListSubscriptionRequests {

    private String subscriptionsRequestId;
    private String businessDocumentsBusinessDocumentTypeId;
    private String businessDocumentsDocumentNumber;
    private String statusId;
    private String unitManagement;
    private String branchId;
    private String fromSubscriptionRequestDate;
    private String toSubscriptionRequestDate;
    private String businessId;
    private String paginationKey;
    private Integer pageSize;

    public String getSubscriptionsRequestId() {
        return subscriptionsRequestId;
    }

    public void setSubscriptionsRequestId(String subscriptionsRequestId) {
        this.subscriptionsRequestId = subscriptionsRequestId;
    }

    public String getBusinessDocumentsBusinessDocumentTypeId() {
        return businessDocumentsBusinessDocumentTypeId;
    }

    public void setBusinessDocumentsBusinessDocumentTypeId(
            String businessDocumentsBusinessDocumentTypeId) {
        this.businessDocumentsBusinessDocumentTypeId = businessDocumentsBusinessDocumentTypeId;
    }

    public String getBusinessDocumentsDocumentNumber() {
        return businessDocumentsDocumentNumber;
    }

    public void setBusinessDocumentsDocumentNumber(
            String businessDocumentsDocumentNumber) {
        this.businessDocumentsDocumentNumber = businessDocumentsDocumentNumber;
    }

    public String getStatusId() {
        return statusId;
    }

    public void setStatusId(String statusId) {
        this.statusId = statusId;
    }

    public String getUnitManagement() {
        return unitManagement;
    }

    public void setUnitManagement(String unitManagement) {
        this.unitManagement = unitManagement;
    }

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public String getFromSubscriptionRequestDate() {
        return fromSubscriptionRequestDate;
    }

    public void setFromSubscriptionRequestDate(
            String fromSubscriptionRequestDate) {
        this.fromSubscriptionRequestDate = fromSubscriptionRequestDate;
    }

    public String getToSubscriptionRequestDate() {
        return toSubscriptionRequestDate;
    }

    public void setToSubscriptionRequestDate(String toSubscriptionRequestDate) {
        this.toSubscriptionRequestDate = toSubscriptionRequestDate;
    }

    public String getBusinessId() {
        return businessId;
    }

    public void setBusinessId(String businessId) {
        this.businessId = businessId;
    }

    public String getPaginationKey() {
        return paginationKey;
    }

    public void setPaginationKey(String paginationKey) {
        this.paginationKey = paginationKey;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }
}