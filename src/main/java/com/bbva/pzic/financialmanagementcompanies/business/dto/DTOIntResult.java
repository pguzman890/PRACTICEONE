package com.bbva.pzic.financialmanagementcompanies.business.dto;

/**
 * Created on 04/03/2020.
 *
 * @author Entelgy
 */
public class DTOIntResult {

    private String id;
    private String reason;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
