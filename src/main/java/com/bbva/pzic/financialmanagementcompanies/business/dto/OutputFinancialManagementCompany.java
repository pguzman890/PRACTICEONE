package com.bbva.pzic.financialmanagementcompanies.business.dto;

import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.Data;

public class OutputFinancialManagementCompany {
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
