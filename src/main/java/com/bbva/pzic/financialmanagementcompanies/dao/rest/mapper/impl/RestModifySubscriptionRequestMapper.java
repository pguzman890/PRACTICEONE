package com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.impl;

import com.bbva.pzic.financialmanagementcompanies.business.dto.InputModifySubscriptionRequest;
import com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests.ModelSubscriptionRequest;
import com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.IRestModifySubscriptionRequestMapper;
import com.bbva.pzic.financialmanagementcompanies.util.mappers.Mapper;
import com.bbva.pzic.financialmanagementcompanies.util.orika.MapperFactory;
import com.bbva.pzic.financialmanagementcompanies.util.orika.impl.ConfigurableMapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * Created on 03/08/2018.
 *
 * @author Entelgy
 */
@Mapper
public class RestModifySubscriptionRequestMapper extends ConfigurableMapper
        implements
        IRestModifySubscriptionRequestMapper {
    static final String SUBSCRIPTION_REQUEST_ID = "subscription-request-id";
    private static final Log LOG = LogFactory
            .getLog(RestListSubscriptionRequestsMapper.class);

    @Override
    public ModelSubscriptionRequest mapIn(InputModifySubscriptionRequest inputModifySubscriptionRequest) {
        LOG.info("... called method RestModifySubscriptionRequestMapper.mapIn ...");
        return map(inputModifySubscriptionRequest, ModelSubscriptionRequest.class);
    }

    @Override
    public Map<String, String> mapInPath(InputModifySubscriptionRequest input) {
        LOG.info("... called method RestModifySubscriptionRequestMapper.mapInPath ...");
        Map<String, String> paths = new HashMap<>();
        paths.put(SUBSCRIPTION_REQUEST_ID, input.getSubscriptionRequestId());
        return paths;
    }

    @Override
    protected void configure(MapperFactory factory) {
        super.configure(factory);
        factory.classMap(InputModifySubscriptionRequest.class,
                ModelSubscriptionRequest.class)
                .field("subscriptionRequestId", "subscriptionRequestId")
                .field("subscriptionRequest.status.id", "status.id")
                .field("subscriptionRequest.status.reason", "status.reason")
                .field("subscriptionRequest.comment", "comment")
                .register();
    }
}