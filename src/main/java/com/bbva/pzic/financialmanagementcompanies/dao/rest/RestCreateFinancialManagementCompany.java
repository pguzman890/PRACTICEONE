package com.bbva.pzic.financialmanagementcompanies.dao.rest;

import com.bbva.pzic.financialmanagementcompanies.business.dto.InputFinancialManagementCompany;
import com.bbva.pzic.financialmanagementcompanies.business.dto.OutputFinancialManagementCompany;
import com.bbva.pzic.financialmanagementcompanies.dao.model.financialManagementCompany.ModelCreateFinancialManagementCompanyRequest;
import com.bbva.pzic.financialmanagementcompanies.dao.model.financialManagementCompany.ModelCreateFinancialManagementCompanyResponse;
import com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.IRestCreateFinancialManagementCompanyMapper;
import com.bbva.pzic.financialmanagementcompanies.util.connection.rest.RestPostConnection;
import org.springframework.beans.factory.annotation.Autowired;
import javax.annotation.PostConstruct;

public class RestCreateFinancialManagementCompany extends RestPostConnection<ModelCreateFinancialManagementCompanyRequest, ModelCreateFinancialManagementCompanyResponse> {
    private static final String CREATE_FINANCIAL_MANAGEMENT_COMPANY_REQUEST = "servicing.url.financialManagementCompanies.createSubscriptionRequest";
    private static final String CREATE_FINANCIAL_MANAGEMENT_COMPANY_REQUEST_USE_PROXY = "servicing.proxy.financialManagementCompanies.createSubscriptionRequest";

    @Autowired
    private IRestCreateFinancialManagementCompanyMapper mapper;

    @PostConstruct
    public void init() {
        useProxy = configurationManager.getBooleanProperty(CREATE_FINANCIAL_MANAGEMENT_COMPANY_REQUEST_USE_PROXY, false);
    }
    public OutputFinancialManagementCompany invoke(final InputFinancialManagementCompany input) {
        return mapper.mapOut(connect(CREATE_FINANCIAL_MANAGEMENT_COMPANY_REQUEST, mapper.mapIn(input)));
    }

    @Override
    protected void evaluateResponse(ModelCreateFinancialManagementCompanyResponse response, int statusCode) {
        //evaluateMessagesResponse(response.getMessages(), "SMCPE1810290", statusCode);

    }
}
