package com.bbva.pzic.financialmanagementcompanies.dao.rest.mock;

import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests.ModelPagination;
import com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests.ModelSubscriptionRequests;
import com.bbva.pzic.financialmanagementcompanies.dao.rest.RestListSubscriptionRequests;
import com.bbva.pzic.financialmanagementcompanies.dao.rest.mock.stubs.ResponseFinancialManagementCompaniesMock;
import com.bbva.pzic.financialmanagementcompanies.util.Errors;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.HashMap;

/**
 * Created on 23/03/2018.
 *
 * @author Entelgy
 */
@Component
@Primary
public class RestListSubscriptionRequestsMock extends RestListSubscriptionRequests {

    public static final String NO_RESPONSE = "9999";
    public static final String NO_PAGINATION = "8888";
    public static final String PAGINATION_EMPTY = "7777";

    @Override
    public ModelSubscriptionRequests connect(final String urlPropertyValue, final HashMap<String, String> params) {
        try {

            final ModelSubscriptionRequests response = ResponseFinancialManagementCompaniesMock.getInstance().buildResponseModelSubscriptionRequests();

            if (params.containsKey("subscriptionsRequest.id")) {
                String subscriptionsRequestId = params.get("subscriptionsRequest.id");
                if (NO_RESPONSE.equals(subscriptionsRequestId)) {
                    return new ModelSubscriptionRequests();
                }
                if (NO_PAGINATION.equals(subscriptionsRequestId)) {
                    response.setPagination(null);
                }
                if (PAGINATION_EMPTY.equals(subscriptionsRequestId)) {
                    response.setPagination(new ModelPagination());
                }
            }
            return response;

        } catch (IOException e) {
            throw new BusinessServiceException(Errors.TECHNICAL_ERROR, e);
        }
    }
}