package com.bbva.pzic.financialmanagementcompanies.dao.model.financialManagementCompany;

public class ModelCreateFinancialManagementCompanyResponse {

    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
