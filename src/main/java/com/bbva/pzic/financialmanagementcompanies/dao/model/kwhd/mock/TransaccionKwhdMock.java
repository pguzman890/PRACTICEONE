package com.bbva.pzic.financialmanagementcompanies.dao.model.kwhd.mock;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.protocolo.ps9.aplicacion.CopySalida;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwhd.FormatoKNECBEHD;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwhd.PeticionTransaccionKwhd;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwhd.RespuestaTransaccionKwhd;
import org.springframework.stereotype.Component;

/**
 * Invocador de la transacci&oacute;n <code>KWHD</code>
 *
 * @see PeticionTransaccionKwhd
 * @see RespuestaTransaccionKwhd
 */
@Component("transaccionKwhd")
public class TransaccionKwhdMock implements InvocadorTransaccion<PeticionTransaccionKwhd, RespuestaTransaccionKwhd> {

    public static final String TEST_NO_RESULT = "99999999";

    @Override
    public RespuestaTransaccionKwhd invocar(PeticionTransaccionKwhd transaccion) {
        RespuestaTransaccionKwhd response = new RespuestaTransaccionKwhd();
        //response.setCodigoRetorno("OK_COMMIT");
        //response.setCodigoControl("OK");

        FormatoKNECBEHD format = transaccion.getCuerpo().getParte(FormatoKNECBEHD.class);
        /*
        if (TEST_NO_RESULT.equals(format.getCodemp())) {
            return response;
        }*/

        CopySalida copy = new CopySalida();
        copy.setCopy(FormatsKwhdMock.getInstance().getFormatoKNECBSHD());
        response.getCuerpo().getPartes().add(copy);
        return response;
    }

    @Override
    public RespuestaTransaccionKwhd invocarCache(PeticionTransaccionKwhd transaccion) {
        return null;
    }

    @Override
    public void vaciarCache() {
    }
}