package com.bbva.pzic.financialmanagementcompanies.dao.model.kwhb;


import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * Formato de datos <code>KNECBEHB</code> de la transacci&oacute;n <code>KWHB</code>
 *
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "KNECBEHB")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoKNECBEHB {

	/**
	 * <p>Campo <code>CODEMP</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "CODEMP", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 8, longitudMaxima = 8)
	private String codemp;

	/**
	 * <p>Campo <code>CTA01</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "CTA01", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String cta01;

	/**
	 * <p>Campo <code>IDEPR1</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3, nombre = "IDEPR1", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
	private String idepr1;

	/**
	 * <p>Campo <code>IDTPR1</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 4, nombre = "IDTPR1", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String idtpr1;

	/**
	 * <p>Campo <code>IDRPR1</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 5, nombre = "IDRPR1", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String idrpr1;

	/**
	 * <p>Campo <code>CTA02</code>, &iacute;ndice: <code>6</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 6, nombre = "CTA02", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String cta02;

	/**
	 * <p>Campo <code>IDEPR2</code>, &iacute;ndice: <code>7</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 7, nombre = "IDEPR2", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
	private String idepr2;

	/**
	 * <p>Campo <code>IDTPR2</code>, &iacute;ndice: <code>8</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 8, nombre = "IDTPR2", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String idtpr2;

	/**
	 * <p>Campo <code>IDRPR2</code>, &iacute;ndice: <code>9</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 9, nombre = "IDRPR2", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String idrpr2;

	/**
	 * <p>Campo <code>CTA03</code>, &iacute;ndice: <code>10</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 10, nombre = "CTA03", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String cta03;

	/**
	 * <p>Campo <code>IDEPR3</code>, &iacute;ndice: <code>11</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 11, nombre = "IDEPR3", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
	private String idepr3;

	/**
	 * <p>Campo <code>IDTPR3</code>, &iacute;ndice: <code>12</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 12, nombre = "IDTPR3", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String idtpr3;

	/**
	 * <p>Campo <code>IDRPR3</code>, &iacute;ndice: <code>13</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 13, nombre = "IDRPR3", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String idrpr3;

	/**
	 * <p>Campo <code>CTA04</code>, &iacute;ndice: <code>14</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 14, nombre = "CTA04", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String cta04;

	/**
	 * <p>Campo <code>IDEPR4</code>, &iacute;ndice: <code>15</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 15, nombre = "IDEPR4", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
	private String idepr4;

	/**
	 * <p>Campo <code>IDTPR4</code>, &iacute;ndice: <code>16</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 16, nombre = "IDTPR4", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String idtpr4;

	/**
	 * <p>Campo <code>IDRPR4</code>, &iacute;ndice: <code>17</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 17, nombre = "IDRPR4", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String idrpr4;

	/**
	 * <p>Campo <code>CTA05</code>, &iacute;ndice: <code>18</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 18, nombre = "CTA05", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String cta05;

	/**
	 * <p>Campo <code>IDEPR5</code>, &iacute;ndice: <code>19</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 19, nombre = "IDEPR5", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
	private String idepr5;

	/**
	 * <p>Campo <code>IDTPR5</code>, &iacute;ndice: <code>20</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 20, nombre = "IDTPR5", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String idtpr5;

	/**
	 * <p>Campo <code>IDRPR5</code>, &iacute;ndice: <code>21</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 21, nombre = "IDRPR5", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String idrpr5;

	/**
	 * <p>Campo <code>CTA06</code>, &iacute;ndice: <code>22</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 22, nombre = "CTA06", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String cta06;

	/**
	 * <p>Campo <code>IDEPR6</code>, &iacute;ndice: <code>23</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 23, nombre = "IDEPR6", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
	private String idepr6;

	/**
	 * <p>Campo <code>IDTPR6</code>, &iacute;ndice: <code>24</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 24, nombre = "IDTPR6", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String idtpr6;

	/**
	 * <p>Campo <code>IDRPR6</code>, &iacute;ndice: <code>25</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 25, nombre = "IDRPR6", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String idrpr6;

	/**
	 * <p>Campo <code>CTA07</code>, &iacute;ndice: <code>26</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 26, nombre = "CTA07", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String cta07;

	/**
	 * <p>Campo <code>IDEPR7</code>, &iacute;ndice: <code>27</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 27, nombre = "IDEPR7", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
	private String idepr7;

	/**
	 * <p>Campo <code>IDTPR7</code>, &iacute;ndice: <code>28</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 28, nombre = "IDTPR7", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String idtpr7;

	/**
	 * <p>Campo <code>IDRPR7</code>, &iacute;ndice: <code>29</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 29, nombre = "IDRPR7", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String idrpr7;

	/**
	 * <p>Campo <code>CTA08</code>, &iacute;ndice: <code>30</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 30, nombre = "CTA08", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String cta08;

	/**
	 * <p>Campo <code>IDEPR8</code>, &iacute;ndice: <code>31</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 31, nombre = "IDEPR8", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
	private String idepr8;

	/**
	 * <p>Campo <code>IDTPR8</code>, &iacute;ndice: <code>32</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 32, nombre = "IDTPR8", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String idtpr8;

	/**
	 * <p>Campo <code>IDRPR8</code>, &iacute;ndice: <code>33</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 33, nombre = "IDRPR8", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String idrpr8;

	/**
	 * <p>Campo <code>CTA09</code>, &iacute;ndice: <code>34</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 34, nombre = "CTA09", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String cta09;

	/**
	 * <p>Campo <code>IDEPR9</code>, &iacute;ndice: <code>35</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 35, nombre = "IDEPR9", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
	private String idepr9;

	/**
	 * <p>Campo <code>IDTPR9</code>, &iacute;ndice: <code>36</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 36, nombre = "IDTPR9", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String idtpr9;

	/**
	 * <p>Campo <code>IDRPR9</code>, &iacute;ndice: <code>37</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 37, nombre = "IDRPR9", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String idrpr9;

	/**
	 * <p>Campo <code>CTA10</code>, &iacute;ndice: <code>38</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 38, nombre = "CTA10", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String cta10;

	/**
	 * <p>Campo <code>IDEPR10</code>, &iacute;ndice: <code>39</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 39, nombre = "IDEPR10", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
	private String idepr10;

	/**
	 * <p>Campo <code>IDTPR10</code>, &iacute;ndice: <code>40</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 40, nombre = "IDTPR10", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String idtpr10;

	/**
	 * <p>Campo <code>IDRPR10</code>, &iacute;ndice: <code>41</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 41, nombre = "IDRPR10", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String idrpr10;

	/**
	 * <p>Campo <code>CTA11</code>, &iacute;ndice: <code>42</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 42, nombre = "CTA11", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String cta11;

	/**
	 * <p>Campo <code>IDEPR11</code>, &iacute;ndice: <code>43</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 43, nombre = "IDEPR11", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
	private String idepr11;

	/**
	 * <p>Campo <code>IDTPR11</code>, &iacute;ndice: <code>44</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 44, nombre = "IDTPR11", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String idtpr11;

	/**
	 * <p>Campo <code>IDRPR11</code>, &iacute;ndice: <code>45</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 45, nombre = "IDRPR11", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String idrpr11;

	/**
	 * <p>Campo <code>CTA12</code>, &iacute;ndice: <code>46</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 46, nombre = "CTA12", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String cta12;

	/**
	 * <p>Campo <code>IDEPR12</code>, &iacute;ndice: <code>47</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 47, nombre = "IDEPR12", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
	private String idepr12;

	/**
	 * <p>Campo <code>IDTPR12</code>, &iacute;ndice: <code>48</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 48, nombre = "IDTPR12", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String idtpr12;

	/**
	 * <p>Campo <code>IDRPR12</code>, &iacute;ndice: <code>49</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 49, nombre = "IDRPR12", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String idrpr12;

	/**
	 * <p>Campo <code>CTA13</code>, &iacute;ndice: <code>50</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 50, nombre = "CTA13", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String cta13;

	/**
	 * <p>Campo <code>IDEPR13</code>, &iacute;ndice: <code>51</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 51, nombre = "IDEPR13", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
	private String idepr13;

	/**
	 * <p>Campo <code>IDTPR13</code>, &iacute;ndice: <code>52</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 52, nombre = "IDTPR13", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String idtpr13;

	/**
	 * <p>Campo <code>IDRPR13</code>, &iacute;ndice: <code>53</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 53, nombre = "IDRPR13", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String idrpr13;

	/**
	 * <p>Campo <code>CTA14</code>, &iacute;ndice: <code>54</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 54, nombre = "CTA14", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String cta14;

	/**
	 * <p>Campo <code>IDEPR14</code>, &iacute;ndice: <code>55</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 55, nombre = "IDEPR14", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
	private String idepr14;

	/**
	 * <p>Campo <code>IDTPR14</code>, &iacute;ndice: <code>56</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 56, nombre = "IDTPR14", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String idtpr14;

	/**
	 * <p>Campo <code>IDRPR14</code>, &iacute;ndice: <code>57</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 57, nombre = "IDRPR14", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String idrpr14;

	/**
	 * <p>Campo <code>CTA15</code>, &iacute;ndice: <code>58</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 58, nombre = "CTA15", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String cta15;

	/**
	 * <p>Campo <code>IDEPR15</code>, &iacute;ndice: <code>59</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 59, nombre = "IDEPR15", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
	private String idepr15;

	/**
	 * <p>Campo <code>IDTPR15</code>, &iacute;ndice: <code>60</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 60, nombre = "IDTPR15", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String idtpr15;

	/**
	 * <p>Campo <code>IDRPR15</code>, &iacute;ndice: <code>61</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 61, nombre = "IDRPR15", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String idrpr15;

}