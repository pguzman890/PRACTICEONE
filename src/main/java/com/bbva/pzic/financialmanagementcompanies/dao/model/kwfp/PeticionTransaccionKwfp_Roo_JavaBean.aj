// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.pzic.financialmanagementcompanies.dao.model.kwfp;

import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwfp.PeticionTransaccionKwfp;

privileged aspect PeticionTransaccionKwfp_Roo_JavaBean {
    
    /**
     * Sets cuerpo value
     * 
     * @param cuerpo
     * @return PeticionTransaccionKwfp
     */
    public PeticionTransaccionKwfp PeticionTransaccionKwfp.setCuerpo(CuerpoMultiparte cuerpo) {
        this.cuerpo = cuerpo;
        return this;
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @return String
     */
    public String PeticionTransaccionKwfp.toString() {
        return "PeticionTransaccionKwfp {" + 
        "}" + super.toString();
    }
    
}
