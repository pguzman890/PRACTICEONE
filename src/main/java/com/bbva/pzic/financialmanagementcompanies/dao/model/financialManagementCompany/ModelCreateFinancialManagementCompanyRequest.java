package com.bbva.pzic.financialmanagementcompanies.dao.model.financialManagementCompany;


import java.util.List;

public class ModelCreateFinancialManagementCompanyRequest {

    private Business business;

    private NetcashType netcashType;

    private Contract contract;

    private Product product;

    private RelationType relationType;

    private List<ReviewerNetcash> reviewers;

    public ModelCreateFinancialManagementCompanyRequest() {
    }

    public Business getBusiness() {
        return business;
    }

    public void setBusiness(Business business) {
        this.business = business;
    }

    public NetcashType getNetcashType() {
        return netcashType;
    }

    public void setNetcashType(NetcashType netcashType) {
        this.netcashType = netcashType;
    }

    public Contract getContract() {
        return contract;
    }

    public void setContract(Contract contract) {
        this.contract = contract;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public RelationType getRelationType() {
        return relationType;
    }

    public void setRelationType(RelationType relationType) {
        this.relationType = relationType;
    }

    public List<ReviewerNetcash> getReviewers() {
        return reviewers;
    }

    public void setReviewers(List<ReviewerNetcash> reviewers) {
        this.reviewers = reviewers;
    }
}
