package com.bbva.pzic.financialmanagementcompanies.dao.model.financialManagementCompany;


import javax.validation.Valid;

public class RelatedProduct {

    private String id;

    @Valid
    private ProductType productType;

    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ProductType getProductType() {
        return productType;
    }

    public void setProductType(ProductType productType) {
        this.productType = productType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
