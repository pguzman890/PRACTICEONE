package com.bbva.pzic.financialmanagementcompanies.dao.model.financialManagementCompany;


public class Product {

    private String id;

    private ProductType productType;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ProductType getProductType() {
        return productType;
    }

    public void setProductType(ProductType productType) {
        this.productType = productType;
    }
}
