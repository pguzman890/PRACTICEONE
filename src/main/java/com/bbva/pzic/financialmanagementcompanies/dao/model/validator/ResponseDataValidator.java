package com.bbva.pzic.financialmanagementcompanies.dao.model.validator;

import com.bbva.jee.arq.spring.core.servicing.gce.xml.instance.ErrorSeverity;
import com.bbva.jee.arq.spring.core.servicing.gce.xml.instance.Message;
import com.bbva.pzic.financialmanagementcompanies.dao.model.ksjo.ResponseData;
import org.springframework.stereotype.Component;

@Component
public class ResponseDataValidator {

    private static final String SUCCESS_CODE = "MDO_000";

    private boolean isNotSuccessCode(final String errorCode) {
        return !(errorCode == null || errorCode.trim().equals(SUCCESS_CODE));
    }

    public Message buildMessage(final ResponseData response) {
        Message message = new Message();
        message.setCode(response.getStatusCode());
        message.setMessage(response.getStatusDescription());
        message.setType(ErrorSeverity.ERROR);
        return message;
    }

    public int buildStatusCode(final ResponseData response, final int statusCode) {
        if (isNotSuccessCode(response.getStatusCode())) {
            return 400;
        } else {
            return statusCode;
        }
    }
}
