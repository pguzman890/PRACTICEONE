package com.bbva.pzic.financialmanagementcompanies.dao.model.kwhb.mock;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwhb.PeticionTransaccionKwhb;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwhb.RespuestaTransaccionKwhb;
import org.springframework.stereotype.Component;

@Component("transaccionKwhb")
public class TransaccionKwhbMock implements InvocadorTransaccion<PeticionTransaccionKwhb, RespuestaTransaccionKwhb> {

    @Override
    public RespuestaTransaccionKwhb invocar(PeticionTransaccionKwhb peticionTransaccionKwhb) throws ExcepcionTransaccion {
        RespuestaTransaccionKwhb response = new RespuestaTransaccionKwhb();
        //response.setCodigoRetorno("OK_COMMIT");
        //response.setCodigoControl("OK");
        return response;
    }

    @Override
    public RespuestaTransaccionKwhb invocarCache(PeticionTransaccionKwhb peticionTransaccionKwhb) throws ExcepcionTransaccion {
        return null;
    }

    @Override
    public void vaciarCache() {

    }
}
