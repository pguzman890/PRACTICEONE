package com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper;

import com.bbva.pzic.financialmanagementcompanies.business.dto.InputModifySubscriptionRequest;
import com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests.ModelSubscriptionRequest;

import java.util.Map;

/**
 * Created on 03/08/2018.
 *
 * @author Entelgy
 */
public interface IRestModifySubscriptionRequestMapper {

    ModelSubscriptionRequest mapIn(InputModifySubscriptionRequest inputModifySubscriptionRequest);

    Map<String, String> mapInPath(InputModifySubscriptionRequest input);
}