package com.bbva.pzic.financialmanagementcompanies.dao.tx.mapper;

import com.bbva.pzic.financialmanagementcompanies.business.dto.DTOIntServiceContract;
import com.bbva.pzic.financialmanagementcompanies.business.dto.InputListFMCAuthorizedBusinessManagersProfiledServicesContracts;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwlc.FormatoKNECLCE0;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwlc.FormatoKNECLCS0;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwlc.FormatoKNECLCS1;

/**
 * Created on 04/03/2020.
 *
 * @author Entelgy
 */
public interface ITxListFMCAuthorizedBusinessManagersProfiledServicesContractsMapper {

    FormatoKNECLCE0 mapIn(InputListFMCAuthorizedBusinessManagersProfiledServicesContracts dtoIn);

    DTOIntServiceContract mapOut(FormatoKNECLCS0 formatOutput, DTOIntServiceContract dtoOut);

    DTOIntServiceContract mapOut2(FormatoKNECLCS1 formatOutput, DTOIntServiceContract dtoOut);
}
