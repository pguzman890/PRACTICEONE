package com.bbva.pzic.financialmanagementcompanies.dao.model.kwfp;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created on 04/03/2020.
 *
 * @author Entelgy
 */
@Component("transaccionKwfp")
public class TransaccionKwfp implements InvocadorTransaccion<PeticionTransaccionKwfp, RespuestaTransaccionKwfp> {

    @Autowired
    private ServicioTransacciones servicioTransacciones;

    @Override
    public RespuestaTransaccionKwfp invocar(PeticionTransaccionKwfp transaccion) throws ExcepcionTransaccion {
        return servicioTransacciones.invocar(PeticionTransaccionKwfp.class, RespuestaTransaccionKwfp.class, transaccion);
    }

    @Override
    public RespuestaTransaccionKwfp invocarCache(PeticionTransaccionKwfp transaccion) throws ExcepcionTransaccion {
        return servicioTransacciones.invocar(PeticionTransaccionKwfp.class, RespuestaTransaccionKwfp.class, transaccion);
    }

    @Override
    public void vaciarCache() {
    }
}
