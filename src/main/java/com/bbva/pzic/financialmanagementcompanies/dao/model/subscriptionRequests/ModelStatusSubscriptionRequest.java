package com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests;

import java.util.Date;

/**
 * @author Entelgy
 */
public class ModelStatusSubscriptionRequest {

    private String id;
    private String name;
    private String reason;
    private Date updateDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
}