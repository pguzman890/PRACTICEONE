package com.bbva.pzic.financialmanagementcompanies.dao.model.financialManagementCompany;


public class NetcashType {

    private String id;

    private Version version;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Version getVersion() {
        return version;
    }

    public void setVersion(Version version) {
        this.version = version;
    }
}
