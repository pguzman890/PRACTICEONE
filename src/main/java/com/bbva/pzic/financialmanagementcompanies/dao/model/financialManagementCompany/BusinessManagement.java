package com.bbva.pzic.financialmanagementcompanies.dao.model.financialManagementCompany;


public class BusinessManagement {

    private ManagementType managementType;


    public ManagementType getManagementType() {
        return managementType;
    }

    public void setManagementType(ManagementType managementType) {
        this.managementType = managementType;
    }


}
