package com.bbva.pzic.financialmanagementcompanies.dao.model.financialManagementCompany;



public class Bank {

    private String id;


    private Branch branch;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Branch getBranch() {
        return branch;
    }

    public void setBranch(Branch branch) {
        this.branch = branch;
    }
}
