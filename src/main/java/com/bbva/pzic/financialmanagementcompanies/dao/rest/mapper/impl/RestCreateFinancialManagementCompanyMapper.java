package com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.impl;

import com.bbva.pzic.financialmanagementcompanies.business.dto.InputFinancialManagementCompany;
import com.bbva.pzic.financialmanagementcompanies.business.dto.OutputFinancialManagementCompany;
import com.bbva.pzic.financialmanagementcompanies.dao.model.financialManagementCompany.*;
import com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.IRestCreateFinancialManagementCompanyMapper;
import com.bbva.pzic.financialmanagementcompanies.util.mappers.Mapper;
import com.bbva.pzic.financialmanagementcompanies.util.orika.MapperFactory;
import com.bbva.pzic.financialmanagementcompanies.util.orika.impl.ConfigurableMapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.List;


@Mapper
public class RestCreateFinancialManagementCompanyMapper implements IRestCreateFinancialManagementCompanyMapper {
    private static final Log LOG = LogFactory
            .getLog(RestCreateFinancialManagementCompanyMapper.class);

    @Override
    public ModelCreateFinancialManagementCompanyRequest mapIn(InputFinancialManagementCompany inputFinancialManagementCompany) {
        LOG.info("... called method RestModifySubscriptionRequestMapper.mapIn ...");
        ModelCreateFinancialManagementCompanyRequest modelCreateFinancialManagementCompanyRequest =  new ModelCreateFinancialManagementCompanyRequest();
        map(inputFinancialManagementCompany, modelCreateFinancialManagementCompanyRequest);
        return modelCreateFinancialManagementCompanyRequest;
    }

    @Override
    public OutputFinancialManagementCompany mapOut(ModelCreateFinancialManagementCompanyResponse response) {
        OutputFinancialManagementCompany outputFinancialManagementCompany =  new OutputFinancialManagementCompany();
        map(response,outputFinancialManagementCompany);

        return outputFinancialManagementCompany;
    }
    public void map(ModelCreateFinancialManagementCompanyResponse modelCreateFinancialManagementCompanyResponse,
                    OutputFinancialManagementCompany outputFinancialManagementCompany){

        outputFinancialManagementCompany.setData(
            new com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.Data(){{
                setId(
                    modelCreateFinancialManagementCompanyResponse.getData().getId()
                );
        }});
    }

    public void map(InputFinancialManagementCompany inputFinancialManagementCompany,
                    ModelCreateFinancialManagementCompanyRequest modelCreateFinancialManagementCompanyRequest){
        Business business= new Business();

        List<BusinessDocument> businessDocuments= new ArrayList<BusinessDocument>();

        business.setBusinessDocuments(businessDocuments);

        BusinessManagement businessManagement= new BusinessManagement();
        ManagementType managementType = new ManagementType();
        managementType.setId(inputFinancialManagementCompany.getBusiness().getBusinessManagement().getManagementType().getId());
        businessManagement.setManagementType( managementType );
        business.setBusinessManagement( businessManagement );

        LimitAmount limitAmount = new LimitAmount();
        limitAmount.setAmount(inputFinancialManagementCompany.getBusiness().getLimitAmount().getAmount());
        limitAmount.setCurrency(inputFinancialManagementCompany.getBusiness().getLimitAmount().getCurrency());
        business.setLimitAmount(limitAmount);
        modelCreateFinancialManagementCompanyRequest.setBusiness(business);

        Contract contract= new Contract();
        contract.setId(inputFinancialManagementCompany.getContract().getId());
        modelCreateFinancialManagementCompanyRequest.setContract(contract);

        NetcashType netcashType = new NetcashType();
        netcashType.setId(inputFinancialManagementCompany.getNetcashType().getId());
        Version version = new Version();
        version.setId(inputFinancialManagementCompany.getNetcashType().getVersion().getId());
        netcashType.setVersion(version);

        modelCreateFinancialManagementCompanyRequest.setNetcashType(netcashType);

        Product product= new Product();
        product.setId(inputFinancialManagementCompany.getProduct().getId());
        ProductType productType = new ProductType();
        productType.setId(inputFinancialManagementCompany.getProduct().getProductType().getId());
        product.setProductType(productType);
        modelCreateFinancialManagementCompanyRequest.setProduct(product);

        List<ReviewerNetcash> reviewers = new ArrayList<ReviewerNetcash>();

        for (com.bbva.pzic.financialmanagementcompanies.dao.model.financialManagementCompany.ReviewerNetcash reviewerNetcash:
                inputFinancialManagementCompany.getReviewers()
        ) {
            reviewers.add(new ReviewerNetcash(){{

                this.setBusinessAgentId(reviewerNetcash.getBusinessAgentId());
                this.setUnitManagement(reviewerNetcash.getUnitManagement());
                this.setRegistrationIdentifier(reviewerNetcash.getRegistrationIdentifier());
                this.setProfessionPosition(reviewerNetcash.getProfessionPosition());

                this.setBank(new Bank(){{
                    this.setId(reviewerNetcash.getBank().getId());
                    this.setBranch(
                            new Branch(){{
                                this.setId(reviewerNetcash.getBank().getBranch().getId());
                            }}
                    );
                }});

                this.setProfile(new Profile(){{
                    this.setId(reviewerNetcash.getProfile().getId());
                }});

                List<ContactDetail> contactDetails= new ArrayList<ContactDetail>();

                for (com.bbva.pzic.financialmanagementcompanies.dao.model.financialManagementCompany.ContactDetail contactDetail:
                        reviewerNetcash.getContactDetails()) {
                    contactDetails.add(new ContactDetail(){{
                        this.setContact(contactDetail.getContact());
                        this.setContactType(contactDetail.getContactType());
                    }});
                }

                this.setContactDetails(contactDetails);

            }});
        }

        modelCreateFinancialManagementCompanyRequest.setReviewers(reviewers);

        RelationType relationType= new RelationType();
        relationType.setId(inputFinancialManagementCompany.getRelationType().getId());
        modelCreateFinancialManagementCompanyRequest.setRelationType(relationType);
    }
}