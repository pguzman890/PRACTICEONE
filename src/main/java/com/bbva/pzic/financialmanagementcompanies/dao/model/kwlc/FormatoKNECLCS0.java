package com.bbva.pzic.financialmanagementcompanies.dao.model.kwlc;

import java.math.BigDecimal;

import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;
import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Formato;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;


/**
 * Formato de datos <code>KNECLCS0</code> de la transacci&oacute;n <code>KWLC</code>
 *
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "KNECLCS0")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoKNECLCS0 {

	/**
	 * <p>Campo <code>TIPOPE</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "TIPOPE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
	private String tipope;

	/**
	 * <p>Campo <code>NUMCON</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@DatoAuditable(omitir = true)
	@Campo(indice = 2, nombre = "NUMCON", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String numcon;

	/**
	 * <p>Campo <code>TIPCON</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3, nombre = "TIPCON", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
	private String tipcon;

	/**
	 * <p>Campo <code>INDFAV</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 4, nombre = "INDFAV", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String indfav;

	/**
	 * <p>Campo <code>ALIAS</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 5, nombre = "ALIAS", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String alias;

	/**
	 * <p>Campo <code>DIVCON</code>, &iacute;ndice: <code>6</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 6, nombre = "DIVCON", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
	private String divcon;

	/**
	 * <p>Campo <code>PAIS</code>, &iacute;ndice: <code>7</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 7, nombre = "PAIS", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
	private String pais;

	/**
	 * <p>Campo <code>IDEBAN</code>, &iacute;ndice: <code>8</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 8, nombre = "IDEBAN", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 4, longitudMaxima = 4)
	private String ideban;

	/**
	 * <p>Campo <code>NOMBAN</code>, &iacute;ndice: <code>9</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 9, nombre = "NOMBAN", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 40, longitudMaxima = 40)
	private String nomban;

	/**
	 * <p>Campo <code>IDEOFI</code>, &iacute;ndice: <code>10</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 10, nombre = "IDEOFI", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 4, longitudMaxima = 4)
	private String ideofi;

	/**
	 * <p>Campo <code>NOMOFI</code>, &iacute;ndice: <code>11</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 11, nombre = "NOMOFI", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 40, longitudMaxima = 40)
	private String nomofi;

	/**
	 * <p>Campo <code>NUMINT</code>, &iacute;ndice: <code>12</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@DatoAuditable(omitir = true)
	@Campo(indice = 12, nombre = "NUMINT", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String numint;

	/**
	 * <p>Campo <code>NUMEXT</code>, &iacute;ndice: <code>13</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@DatoAuditable(omitir = true)
	@Campo(indice = 13, nombre = "NUMEXT", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String numext;

	/**
	 * <p>Campo <code>TIPPER</code>, &iacute;ndice: <code>14</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 14, nombre = "TIPPER", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
	private String tipper;

	/**
	 * <p>Campo <code>DESTIP</code>, &iacute;ndice: <code>15</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 15, nombre = "DESTIP", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 40, longitudMaxima = 40)
	private String destip;

	/**
	 * <p>Campo <code>PODVAL</code>, &iacute;ndice: <code>16</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 16, nombre = "PODVAL", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
	private String podval;

	/**
	 * <p>Campo <code>DESPOD</code>, &iacute;ndice: <code>17</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 17, nombre = "DESPOD", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 40, longitudMaxima = 40)
	private String despod;

	/**
	 * <p>Campo <code>LIMOPE</code>, &iacute;ndice: <code>18</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 18, nombre = "LIMOPE", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, signo = true, decimales = 2)
	private BigDecimal limope;

	/**
	 * <p>Campo <code>DIVLIM</code>, &iacute;ndice: <code>19</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 19, nombre = "DIVLIM", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
	private String divlim;

	/**
	 * <p>Campo <code>NOMTITU</code>, &iacute;ndice: <code>20</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 20, nombre = "NOMTITU", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 60, longitudMaxima = 60)
	private String nomtitu;

	/**
	 * <p>Campo <code>SALDISP</code>, &iacute;ndice: <code>21</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 21, nombre = "SALDISP", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, signo = true, decimales = 2)
	private BigDecimal saldisp;

	/**
	 * <p>Campo <code>DIVCUE</code>, &iacute;ndice: <code>22</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 22, nombre = "DIVCUE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
	private String divcue;

	/**
	 * <p>Campo <code>CODPRO</code>, &iacute;ndice: <code>23</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 23, nombre = "CODPRO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
	private String codpro;

	/**
	 * <p>Campo <code>NOMPRO</code>, &iacute;ndice: <code>24</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 24, nombre = "NOMPRO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String nompro;

	/**
	 * <p>Campo <code>IDEPRO</code>, &iacute;ndice: <code>25</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 25, nombre = "IDEPRO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String idepro;

	/**
	 * <p>Campo <code>DESPRO</code>, &iacute;ndice: <code>26</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 26, nombre = "DESPRO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String despro;

    public String getTipope() {
    	return tipope;
    }

	public String getTipcon() {
    	return tipcon;
	}

	public String getTipper() {
    	return tipper;
	}

	public String getPodval() {
    	return podval;
	}

	public String getIdepro() {
    	return idepro;
	}
}
