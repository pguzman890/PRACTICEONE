package com.bbva.pzic.financialmanagementcompanies.dao.model.financialManagementCompany;


import java.util.List;


public class ReviewerNetcash {

    private String businessAgentId;

    private List<ContactDetail> contactDetails;

    private ReviewerType reviewerType;

    private String unitManagement;

    private Bank bank;

    private Profile profile;

    private String professionPosition;

    private String registrationIdentifier;

    public ReviewerType getReviewerType() {
        return reviewerType;
    }

    public void setReviewerType(ReviewerType reviewerType) {
        this.reviewerType = reviewerType;
    }

    public String getUnitManagement() {
        return unitManagement;
    }

    public void setUnitManagement(String unitManagement) {
        this.unitManagement = unitManagement;
    }

    public Bank getBank() {
        return bank;
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public String getProfessionPosition() {
        return professionPosition;
    }

    public void setProfessionPosition(String professionPosition) {
        this.professionPosition = professionPosition;
    }

    public String getRegistrationIdentifier() {
        return registrationIdentifier;
    }

    public void setRegistrationIdentifier(String registrationIdentifier) {
        this.registrationIdentifier = registrationIdentifier;
    }

    public List<ContactDetail> getContactDetails() {
        return contactDetails;
    }

    public void setContactDetails(List<ContactDetail> contactDetails) {
        this.contactDetails = contactDetails;
    }

    public String getBusinessAgentId() {
        return businessAgentId;
    }

    public void setBusinessAgentId(String businessAgentId) {
        this.businessAgentId = businessAgentId;
    }
}
