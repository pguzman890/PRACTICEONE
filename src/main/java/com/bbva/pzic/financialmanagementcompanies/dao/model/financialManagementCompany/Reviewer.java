package com.bbva.pzic.financialmanagementcompanies.dao.model.financialManagementCompany;

import java.util.List;


public class Reviewer {

    private String businessAgentId;

    private String firstName;

    private String middleName;

    private String lastName;

    private String secondLastName;

    private List<ContactDetail> contactDetails;

    private ReviewerType reviewerType;

    public String getBusinessAgentId() {
        return businessAgentId;
    }

    public void setBusinessAgentId(String businessAgentId) {
        this.businessAgentId = businessAgentId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSecondLastName() {
        return secondLastName;
    }

    public void setSecondLastName(String secondLastName) {
        this.secondLastName = secondLastName;
    }

    public List<ContactDetail> getContactDetails() {
        return contactDetails;
    }

    public void setContactDetails(List<ContactDetail> contactDetails) {
        this.contactDetails = contactDetails;
    }

    public ReviewerType getReviewerType() {
        return reviewerType;
    }

    public void setReviewerType(ReviewerType reviewerType) {
        this.reviewerType = reviewerType;
    }
}
