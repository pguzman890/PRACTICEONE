package com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.common.impl;

import com.bbva.pzic.financialmanagementcompanies.business.dto.DTOIntRequestBody;
import com.bbva.pzic.financialmanagementcompanies.business.dto.DTOIntRequestHeader;
import com.bbva.pzic.financialmanagementcompanies.dao.model.ksjo.BodyDataRest;
import com.bbva.pzic.financialmanagementcompanies.dao.model.ksjo.DataOperation;
import com.bbva.pzic.financialmanagementcompanies.dao.model.ksjo.DataOptionals;
import com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.common.IRequestBackendMapper;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class RequestBackendMapper implements IRequestBackendMapper {

    private static final String AAP = "aap";
    private static final String SERVICE_ID = "serviceid";
    private static final String REQUEST_ID = "requestid";
    private static final String CONTACT_ID = "contactid";
    private static final String USUARIO = "usuario";

    @Override
    public BodyDataRest mapInBodyDataRest(DTOIntRequestBody requestBody) {
        BodyDataRest bodyDataRest = new BodyDataRest();
        bodyDataRest.setCountry(requestBody.getCountry());
        bodyDataRest.setOperation(requestBody.getOperation());
        bodyDataRest.setDataOperation(getDataOperation(requestBody));
        bodyDataRest.setDataOptionals(getDataOptionals(requestBody));
        return bodyDataRest;
    }

    @Override
    public Map<String, String> mapInHeaderDataRest(final DTOIntRequestHeader requestHeader) {
        Map<String, String> header = new HashMap<>();
        header.put(AAP, requestHeader.getAap());
        header.put(SERVICE_ID, requestHeader.getServiceID());
        header.put(REQUEST_ID, requestHeader.getRequestID());
        header.put(CONTACT_ID, requestHeader.getContactID());
        header.put(USUARIO, requestHeader.getUser());
        return header;
    }

    private DataOperation getDataOperation(DTOIntRequestBody input) {
        DataOperation dataOperation = new DataOperation();
        dataOperation.setAlias(input.getDataOperationAlias());
        dataOperation.setBank(input.getDataOperationBank());
        return dataOperation;
    }

    private DataOptionals getDataOptionals(DTOIntRequestBody request) {
        DataOptionals dataOptionals = new DataOptionals();
        dataOptionals.setLdap(request.getDataOptionalsLdap());
        dataOptionals.setRama(request.getDataOptionalsRama());
        dataOptionals.setGrupo(request.getDataOptionalsGrupo());
        dataOptionals.setPrefijo(request.getDataOptionalsPrefijo());
        return dataOptionals;
    }
}
