package com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests;

import java.util.List;

/**
 * @author Entelgy
 */
public class ModelReviewerSubscriptionRequest {

    private ModelForm form;
    private ModelParticipant participant;
    private ModelParticipantType participantType;
    private String firstName;
    private String middleName;
    private String lastName;
    private String motherLastName;
    private List<ModelContactInformation> contactInformations;

    public ModelForm getForm() {
        return form;
    }

    public void setForm(ModelForm form) {
        this.form = form;
    }

    public ModelParticipant getParticipant() {
        return participant;
    }

    public void setParticipant(ModelParticipant participant) {
        this.participant = participant;
    }

    public ModelParticipantType getParticipantType() {
        return participantType;
    }

    public void setParticipantType(ModelParticipantType participantType) {
        this.participantType = participantType;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMotherLastName() {
        return motherLastName;
    }

    public void setMotherLastName(String motherLastName) {
        this.motherLastName = motherLastName;
    }

    public List<ModelContactInformation> getContactInformations() {
        return contactInformations;
    }

    public void setContactInformations(List<ModelContactInformation> contactInformations) {
        this.contactInformations = contactInformations;
    }

}
