package com.bbva.pzic.financialmanagementcompanies.dao.model.financialManagementCompany;

import java.util.Date;

public class BusinessDocument {


    private BusinessDocumentType businessDocumentType;

    private String documentNumber;

    private Date issueDate;

    private Date expirationDate;

    public BusinessDocumentType getBusinessDocumentType() {
        return businessDocumentType;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public Date getIssueDate() {
        return issueDate;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setBusinessDocumentType(
            BusinessDocumentType businessDocumentType) {
        this.businessDocumentType = businessDocumentType;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public void setIssueDate(Date issueDate) {
        this.issueDate = issueDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }
}
