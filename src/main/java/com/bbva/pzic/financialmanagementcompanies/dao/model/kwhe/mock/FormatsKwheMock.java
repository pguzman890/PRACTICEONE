package com.bbva.pzic.financialmanagementcompanies.dao.model.kwhe.mock;

import com.bbva.pzic.financialmanagementcompanies.dao.model.kwhe.FormatoKNECBSHE;
import com.bbva.pzic.financialmanagementcompanies.util.mappers.ObjectMapperHelper;

import java.io.IOException;

/**
 * Created on 18/09/2018.
 *
 * @author Entelgy
 */
public final class FormatsKwheMock {

    private static final FormatsKwheMock INSTANCE = new FormatsKwheMock();
    private ObjectMapperHelper objectMapper = ObjectMapperHelper.getInstance();

    private FormatsKwheMock() {
    }

    public static FormatsKwheMock getInstance() {
        return INSTANCE;
    }

    public FormatoKNECBSHE getFormatoKNECBSHE() throws IOException {
        return objectMapper
                .readValue(
                        Thread.currentThread()
                                .getContextClassLoader()
                                .getResourceAsStream(
                                        "com/bbva/pzic/financialmanagementcompanies/dao/model/kwhe/mock/formatoKNECBSHE.json"),
                        FormatoKNECBSHE.class);
    }
}