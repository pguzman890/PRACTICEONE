package com.bbva.pzic.financialmanagementcompanies.dao.model.financialManagementCompany;


import javax.validation.Valid;
import java.io.Serializable;
import java.util.List;

public class Business implements Serializable {

    @Valid
    private List<BusinessDocument> businessDocuments;

    @Valid
    private BusinessManagement businessManagement;

    @Valid
    private LimitAmount limitAmount;

    public List<BusinessDocument> getBusinessDocuments() {
        return businessDocuments;
    }

    public BusinessManagement getBusinessManagement() {
        return businessManagement;
    }

    public LimitAmount getLimitAmount() {
        return limitAmount;
    }

    public void setBusinessDocuments(List<BusinessDocument> businessDocuments) {
        this.businessDocuments = businessDocuments;
    }

    public void setBusinessManagement(BusinessManagement businessManagement) {
        this.businessManagement = businessManagement;
    }

    public void setLimitAmount(LimitAmount limitAmount) {
        this.limitAmount = limitAmount;
    }
}
