package com.bbva.pzic.financialmanagementcompanies.dao.tx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.financialmanagementcompanies.business.dto.InputCreateAuthorizedBusinessManager;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwhe.FormatoKNECBEHE;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwhe.FormatoKNECBSHE;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwhe.PeticionTransaccionKwhe;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwhe.RespuestaTransaccionKwhe;
import com.bbva.pzic.financialmanagementcompanies.dao.tx.mapper.ITxCreateFinancialManagementCompaniesAuthorizedBusinessManagerMapper;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.CreateAuthorizedBusinessManager;
import com.bbva.pzic.routine.commons.utils.host.templates.impl.SingleOutputFormat;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Created on 18/09/2018.
 *
 * @author Entelgy
 */
@Component("txCreateFinancialManagementCompaniesAuthorizedBusinessManager")
public class TxCreateFinancialManagementCompaniesAuthorizedBusinessManager
        extends SingleOutputFormat<InputCreateAuthorizedBusinessManager, FormatoKNECBEHE, CreateAuthorizedBusinessManager, FormatoKNECBSHE> {

    @Resource(name = "txCreateFinancialManagementCompaniesAuthorizedBusinessManagerMapper")
    private ITxCreateFinancialManagementCompaniesAuthorizedBusinessManagerMapper mapper;

    public TxCreateFinancialManagementCompaniesAuthorizedBusinessManager(@Qualifier("transaccionKwhe") InvocadorTransaccion<PeticionTransaccionKwhe, RespuestaTransaccionKwhe> transaction) {
        super(transaction, PeticionTransaccionKwhe::new, CreateAuthorizedBusinessManager::new, FormatoKNECBSHE.class);
    }

    @Override
    protected FormatoKNECBEHE mapInput(InputCreateAuthorizedBusinessManager inputCreateAuthorizedBusinessManager) {
        return mapper.mapIn(inputCreateAuthorizedBusinessManager);
    }

    @Override
    protected CreateAuthorizedBusinessManager mapFirstOutputFormat(FormatoKNECBSHE formatoKNECBSHE, InputCreateAuthorizedBusinessManager inputCreateAuthorizedBusinessManager, CreateAuthorizedBusinessManager createAuthorizedBusinessManager) {
        return mapper.mapOut(formatoKNECBSHE);
    }
}
