package com.bbva.pzic.financialmanagementcompanies.dao.tx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.financialmanagementcompanies.business.dto.DTOIntServiceContract;
import com.bbva.pzic.financialmanagementcompanies.business.dto.InputListFMCAuthorizedBusinessManagersProfiledServicesContracts;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwlc.*;
import com.bbva.pzic.financialmanagementcompanies.dao.tx.mapper.ITxListFMCAuthorizedBusinessManagersProfiledServicesContractsMapper;
import com.bbva.pzic.routine.commons.utils.host.templates.impl.DoubleOutputFormat;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Created on 04/03/2020.
 *
 * @author Entelgy
 */
@Component("txListFinancialManagementCompaniesAuthorizedBusinessManagersProfiledServicesContracts")
public class TxListFMCAuthorizedBusinessManagersProfiledServicesContracts
        extends DoubleOutputFormat<InputListFMCAuthorizedBusinessManagersProfiledServicesContracts, FormatoKNECLCE0, DTOIntServiceContract, FormatoKNECLCS0, FormatoKNECLCS1> {

    @Resource(name = "txListFMCAuthorizedBusinessManagersProfiledServicesContractsMapper")
    private ITxListFMCAuthorizedBusinessManagersProfiledServicesContractsMapper mapper;

    public TxListFMCAuthorizedBusinessManagersProfiledServicesContracts(@Qualifier("transaccionKwlc") InvocadorTransaccion<PeticionTransaccionKwlc, RespuestaTransaccionKwlc> transaction) {
        super(transaction, PeticionTransaccionKwlc::new, DTOIntServiceContract::new, FormatoKNECLCS0.class, FormatoKNECLCS1.class);
    }

    @Override
    protected FormatoKNECLCE0 mapInput(InputListFMCAuthorizedBusinessManagersProfiledServicesContracts inputListFMCAuthorizedBusinessManagersProfiledServicesContracts) {
        return mapper.mapIn(inputListFMCAuthorizedBusinessManagersProfiledServicesContracts);
    }

    @Override
    protected DTOIntServiceContract mapFirstOutputFormat(FormatoKNECLCS0 formatoKNECLCS0, InputListFMCAuthorizedBusinessManagersProfiledServicesContracts inputListFMCAuthorizedBusinessManagersProfiledServicesContracts, DTOIntServiceContract dtoIntServiceContract) {
        return mapper.mapOut(formatoKNECLCS0, dtoIntServiceContract);
    }

    @Override
    protected DTOIntServiceContract mapSecondOutputFormat(FormatoKNECLCS1 formatoKNECLCS1, InputListFMCAuthorizedBusinessManagersProfiledServicesContracts inputListFMCAuthorizedBusinessManagersProfiledServicesContracts, DTOIntServiceContract dtoIntServiceContract) {
        return mapper.mapOut2(formatoKNECLCS1, dtoIntServiceContract);
    }
}
