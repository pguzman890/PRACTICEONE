package com.bbva.pzic.financialmanagementcompanies.dao.rest.mock;

import com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests.ModelSubscriptionRequest;
import com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests.ModelSubscriptionResponse;
import com.bbva.pzic.financialmanagementcompanies.dao.rest.RestCreateSubscriptionRequest;
import com.bbva.pzic.financialmanagementcompanies.dao.rest.mock.stubs.ResponseFinancialManagementCompaniesMock;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

/**
 * Created on 03/08/2018.
 *
 * @author Entelgy
 */
@Primary
@Component
public class RestCreateSubscriptionRequestMock extends RestCreateSubscriptionRequest {

    @Override
    protected ModelSubscriptionResponse connect(String urlPropertyValue, ModelSubscriptionRequest entityPayload) {
        return ResponseFinancialManagementCompaniesMock.getInstance().buildModelSubscriptionResponse();
    }
}