package com.bbva.pzic.financialmanagementcompanies.dao.model.kwhe;


import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * Formato de datos <code>KNECBEHE</code> de la transacci&oacute;n <code>KWHE</code>
 *
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "KNECBEHE")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoKNECBEHE {

	/**
	 * <p>Campo <code>CODEMP</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "CODEMP", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 8, longitudMaxima = 8)
	private String codemp;

	/**
	 * <p>Campo <code>USUID</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "USUID", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 24, longitudMaxima = 24)
	private String usuid;

	/**
	 * <p>Campo <code>PODVAL</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3, nombre = "PODVAL", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String podval;

	/**
	 * <p>Campo <code>TIPPER</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 4, nombre = "TIPPER", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String tipper;

	/**
	 * <p>Campo <code>ESTGER</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 5, nombre = "ESTGER", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
	private String estger;

}