package com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.impl;

import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.*;
import com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests.*;
import com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.IRestCreateSubscriptionRequestMapper;
import com.bbva.pzic.financialmanagementcompanies.util.mappers.Mapper;
import com.bbva.pzic.financialmanagementcompanies.util.orika.MapperFactory;
import com.bbva.pzic.financialmanagementcompanies.util.orika.impl.ConfigurableMapper;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 03/08/2018.
 *
 * @author Entelgy
 */
@Mapper
public class RestCreateSubscriptionRequestMapper extends ConfigurableMapper
        implements IRestCreateSubscriptionRequestMapper {

    private static final Log LOG = LogFactory.getLog(RestCreateSubscriptionRequestMapper.class);

    private static final String PRODUCT_ID = "product.id";
    private static final String DOCUMENT_NUMBER = "documentNumber";

    @Override
    protected void configure(MapperFactory factory) {
        super.configure(factory);

        factory.classMap(SubscriptionRequest.class, ModelSubscriptionRequest.class)
                .field("business.id", "business.id")
                .field("business.businessManagement.managementType.id", "business.businessManagement.managementType.id")
                .field("business.legalName", "business.legalName")
                .field("limitAmount.amount", "limitAmount.amount")
                .field("limitAmount.currency", "limitAmount.currency")
                .field(PRODUCT_ID, PRODUCT_ID)
                .field("joint.id", "joint.id")
                .field("unitManagement", "unitManagement")
                .field("branch.id", "branch.id")
                .register();

        factory.classMap(BusinessDocumentSubscriptionRequest.class, ModelBusinessDocumentSubscriptionRequest.class)
                .field("businessDocumentType.id", "businessDocumentType.id")
                .field(DOCUMENT_NUMBER, DOCUMENT_NUMBER)
                .register();

        factory.classMap(BusinessManagerSubscriptionRequest.class, ModelBusinessManagerSubscriptionRequest.class)
                .field("firstName", "firstName")
                .field("middleName", "middleName")
                .field("lastName", "lastName")
                .field("secondLastName", "secondLastName")
                .register();

        factory.classMap(IdentityDocumentSubscriptionRequest.class, ModelIdentityDocumentSubscriptionRequest.class)
                .field("documentType.id", "documentType.id")
                .field(DOCUMENT_NUMBER, DOCUMENT_NUMBER)
                .register();

        factory.classMap(ContactDetail.class, ModelContactDetail.class)
                .field("contactType", "contactType")
                .field("contact", "contact")
                .register();

        factory.classMap(RelatedContract.class, ModelRelatedContract.class)
                .field("contract.id", "contract.id")
                .field(PRODUCT_ID, PRODUCT_ID)
                .field("product.productType.id", "product.productType.id")
                .field("relationType.id", "relationType.id")
                .register();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ModelSubscriptionRequest mapIn(
            final SubscriptionRequest subscriptionRequest) {
        LOG.info("... called method RestCreateSubscriptionRequestMapper.mapIn ...");
        ModelSubscriptionRequest dtoInt = map(subscriptionRequest, ModelSubscriptionRequest.class);

        dtoInt.setBusiness(mapBusiness(dtoInt.getBusiness(), subscriptionRequest.getBusiness()));
        dtoInt.setRelatedContracts(mapRelatedContracts(subscriptionRequest.getRelatedContracts()));
        dtoInt.setBusinessManagers(mapBusinessManagers(subscriptionRequest.getBusinessManagers()));
        return dtoInt;
    }

    @Override
    public String mapOut(final ModelSubscriptionResponse response) {
        if (response == null) {
            return null;
        } else {
            return response.getId();
        }
    }

    private ModelBusinessSubscriptionRequest mapBusiness(final ModelBusinessSubscriptionRequest dto, final BusinessSubscriptionRequest canonic) {
        if (canonic == null || CollectionUtils.isEmpty(canonic.getBusinessDocuments())) {
            return dto;
        }
        ModelBusinessSubscriptionRequest dtoMapped = dto;
        if (dto == null) {
            dtoMapped = new ModelBusinessSubscriptionRequest();
        }
        List<ModelBusinessDocumentSubscriptionRequest> dtos = new ArrayList<>();
        for (BusinessDocumentSubscriptionRequest businessDocumentSubscriptionRequest : canonic.getBusinessDocuments()) {
            dtos.add(map(businessDocumentSubscriptionRequest, ModelBusinessDocumentSubscriptionRequest.class));
        }
        dtoMapped.setBusinessDocuments(dtos);
        return dtoMapped;
    }

    private List<ModelRelatedContract> mapRelatedContracts(List<RelatedContract> relatedContracts) {
        if (CollectionUtils.isEmpty(relatedContracts)) {
            return null;
        }
        List<ModelRelatedContract> dtos = new ArrayList<>();
        for (RelatedContract canonic : relatedContracts) {
            dtos.add(map(canonic, ModelRelatedContract.class));
        }
        return dtos;
    }

    private List<ModelBusinessManagerSubscriptionRequest> mapBusinessManagers(List<BusinessManagerSubscriptionRequest> canonics) {
        if (CollectionUtils.isEmpty(canonics)) {
            return null;
        }
        List<ModelBusinessManagerSubscriptionRequest> dtos = new ArrayList<>();
        for (BusinessManagerSubscriptionRequest canonic : canonics) {
            ModelBusinessManagerSubscriptionRequest dto = map(canonic, ModelBusinessManagerSubscriptionRequest.class);
            dto.setIdentityDocuments(mapIdentityDocuments(canonic.getIdentityDocuments()));
            dto.setContactDetails(mapContactDetails(canonic.getContactDetails()));
            dto.setRoles(mapRole(canonic.getRoles()));
            dtos.add(dto);
        }
        return dtos;
    }

    private List<ModelRole> mapRole(List<Role> canonics) {
        if (CollectionUtils.isEmpty(canonics)) {
            return null;
        }
        List<ModelRole> dtos = new ArrayList<>();
        for (Role canonic : canonics) {
            ModelRole dto = new ModelRole();
            dto.setId(canonic.getId());
            dtos.add(dto);
        }
        return dtos;
    }

    private List<ModelIdentityDocumentSubscriptionRequest> mapIdentityDocuments(List<IdentityDocumentSubscriptionRequest> canonics) {
        if (CollectionUtils.isEmpty(canonics)) {
            return null;
        }
        List<ModelIdentityDocumentSubscriptionRequest> dtos = new ArrayList<>();
        for (IdentityDocumentSubscriptionRequest canonic : canonics) {
            dtos.add(map(canonic, ModelIdentityDocumentSubscriptionRequest.class));
        }
        return dtos;
    }

    private List<ModelContactDetail> mapContactDetails(List<ContactDetail> canonics) {
        if (CollectionUtils.isEmpty(canonics)) {
            return null;
        }
        List<ModelContactDetail> dtos = new ArrayList<>();
        for (ContactDetail canonic : canonics) {
            dtos.add(map(canonic, ModelContactDetail.class));
        }
        return dtos;
    }
}
